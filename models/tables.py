# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.


def get_user_email():
    return auth.user.email if auth.user else None


db.define_table('alameda_info',
                Field('artist'),
                Field('city'),
                Field('url', 'text'),
                )

db.define_table('alpine_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('amador_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('butte_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('calaveras_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('colusa_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('contracosta_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('delnorte_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('eldorado_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('fresno_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('glenn_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('humboldt_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('imperial_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('inyo_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('kern_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('kings_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('lake_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('lassen_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('losangeles_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('madera_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('marin_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('mariposa_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('mendocino_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('merced_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('modoc_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('mono_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('monterey_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )
db.define_table('napa_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('nevada_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('orange_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('placer_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('plumas_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('riverside_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('sacramento_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('sanbenito_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('sanbernardino_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('sandiego_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )
db.define_table('sanfrancisco_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('sanjoaquin_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('sanluisobispo_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('sanmateo_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('santabarbara_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('santaclara_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('santacruz_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('shasta_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('sierra_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('siskiyou_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )
db.define_table('solano_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('sonoma_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('stanislaus_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('sutter_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('tehama_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('trinity_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('tulare_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('tuolumne_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('ventura_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('yolo_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

db.define_table('yuba_info',
                Field('artist'),
                Field('city'),
                Field('url'),
                )

# after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)
