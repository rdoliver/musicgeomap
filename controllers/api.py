# Here go your api methods.


def get_alameda_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    alameda_info = []
    rows = db().select(db.alameda_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            alameda_info.append(t)
    return response.json(dict(
        alameda_info=alameda_info,
    ))


@auth.requires_signature()
def add_alameda_info():
    i_id = db.alameda_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(alameda_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_alameda_info():
    db(db.alameda_info.id == request.vars.alameda_info_id).delete()
    return "ok"


def get_alpine_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    alpine_info = []
    rows = db().select(db.alpine_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            alpine_info.append(t)
    return response.json(dict(
        alpine_info=alpine_info,
    ))


@auth.requires_signature()
def add_alpine_info():
    i_id = db.alpine_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(alpine_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_alpine_info():
    db(db.alpine_info.id == request.vars.alpine_info_id).delete()
    return "ok"


def get_amador_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    amador_info = []
    rows = db().select(db.amador_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            amador_info.append(t)
    return response.json(dict(
        amador_info=amador_info,
    ))


@auth.requires_signature()
def add_amador_info():
    i_id = db.amador_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(amador_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_amador_info():
    db(db.amador_info.id == request.vars.amador_info_id).delete()
    return "ok"


def get_butte_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    butte_info = []
    rows = db().select(db.butte_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            butte_info.append(t)
    return response.json(dict(
        butte_info=butte_info,
    ))


@auth.requires_signature()
def add_butte_info():
    i_id = db.butte_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(butte_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_butte_info():
    db(db.butte_info.id == request.vars.butte_info_id).delete()
    return "ok"


def get_calaveras_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    calaveras_info = []
    rows = db().select(db.calaveras_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            calaveras_info.append(t)
    return response.json(dict(
        calaveras_info=calaveras_info,
    ))


@auth.requires_signature()
def add_calaveras_info():
    i_id = db.calaveras_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(calaveras_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_calaveras_info():
    db(db.calaveras_info.id == request.vars.calaveras_info_id).delete()
    return "ok"


def get_colusa_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    colusa_info = []
    rows = db().select(db.colusa_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            colusa_info.append(t)
    return response.json(dict(
        colusa_info=colusa_info,
    ))


@auth.requires_signature()
def add_colusa_info():
    i_id = db.colusa_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(colusa_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_colusa_info():
    db(db.colusa_info.id == request.vars.colusa_info_id).delete()
    return "ok"


def get_contracosta_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    contracosta_info = []
    rows = db().select(db.contracosta_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            contracosta_info.append(t)
    return response.json(dict(
        contracosta_info=contracosta_info,
    ))


@auth.requires_signature()
def add_contracosta_info():
    i_id = db.contracosta_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(contracosta_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_contracosta_info():
    db(db.contracosta_info.id == request.vars.contracosta_info_id).delete()
    return "ok"


def get_delnorte_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    delnorte_info = []
    rows = db().select(db.delnorte_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            delnorte_info.append(t)
    return response.json(dict(
        delnorte_info=delnorte_info,
    ))


@auth.requires_signature()
def add_delnorte_info():
    i_id = db.delnorte_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(delnorte_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_delnorte_info():
    db(db.delnorte_info.id == request.vars.delnorte_info_id).delete()
    return "ok"


def get_eldorado_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    eldorado_info = []
    rows = db().select(db.eldorado_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            eldorado_info.append(t)
    return response.json(dict(
        eldorado_info=eldorado_info,
    ))


@auth.requires_signature()
def add_eldorado_info():
    i_id = db.eldorado_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(eldorado_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_eldorado_info():
    db(db.eldorado_info.id == request.vars.eldorado_info_id).delete()
    return "ok"


def get_fresno_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    fresno_info = []
    rows = db().select(db.fresno_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            fresno_info.append(t)
    return response.json(dict(
        fresno_info=fresno_info,
    ))


@auth.requires_signature()
def add_fresno_info():
    i_id = db.fresno_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(fresno_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_fresno_info():
    db(db.fresno_info.id == request.vars.fresno_info_id).delete()
    return "ok"


def get_glenn_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    glenn_info = []
    rows = db().select(db.glenn_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            glenn_info.append(t)
    return response.json(dict(
        glenn_info=glenn_info,
    ))


@auth.requires_signature()
def add_glenn_info():
    i_id = db.glenn_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(glenn_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_glenn_info():
    db(db.glenn_info.id == request.vars.glenn_info_id).delete()
    return "ok"


def get_humboldt_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    humboldt_info = []
    rows = db().select(db.humboldt_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            humboldt_info.append(t)
    return response.json(dict(
        humboldt_info=humboldt_info,
    ))


@auth.requires_signature()
def add_humboldt_info():
    i_id = db.humboldt_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(humboldt_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_humboldt_info():
    db(db.humboldt_info.id == request.vars.humboldt_info_id).delete()
    return "ok"


def get_imperial_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    imperial_info = []
    rows = db().select(db.imperial_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            imperial_info.append(t)
    return response.json(dict(
        imperial_info=imperial_info,
    ))


@auth.requires_signature()
def add_imperial_info():
    i_id = db.imperial_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(imperial_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_imperial_info():
    db(db.imperial_info.id == request.vars.imperial_info_id).delete()
    return "ok"


def get_inyo_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    inyo_info = []
    rows = db().select(db.inyo_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            inyo_info.append(t)
    return response.json(dict(
        inyo_info=inyo_info,
    ))


@auth.requires_signature()
def add_inyo_info():
    i_id = db.inyo_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(inyo_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_inyo_info():
    db(db.inyo_info.id == request.vars.inyo_info_id).delete()
    return "ok"


def get_kern_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    kern_info = []
    rows = db().select(db.kern_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            kern_info.append(t)
    return response.json(dict(
        kern_info=kern_info,
    ))


@auth.requires_signature()
def add_kern_info():
    i_id = db.kern_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(kern_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_kern_info():
    db(db.kern_info.id == request.vars.kern_info_id).delete()
    return "ok"


def get_kings_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    kings_info = []
    rows = db().select(db.kings_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            kings_info.append(t)
    return response.json(dict(
        kings_info=kings_info,
    ))


@auth.requires_signature()
def add_kings_info():
    i_id = db.kings_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(kings_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_kings_info():
    db(db.kings_info.id == request.vars.kings_info_id).delete()
    return "ok"


def get_lake_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    lake_info = []
    rows = db().select(db.lake_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            lake_info.append(t)
    return response.json(dict(
        lake_info=lake_info,
    ))


@auth.requires_signature()
def add_lake_info():
    i_id = db.lake_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(lake_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_lake_info():
    db(db.lake_info.id == request.vars.lake_info_id).delete()
    return "ok"


def get_lassen_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    lassen_info = []
    rows = db().select(db.lassen_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            lassen_info.append(t)
    return response.json(dict(
        lassen_info=lassen_info,
    ))


@auth.requires_signature()
def add_lassen_info():
    i_id = db.lassen_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(lassen_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_lassen_info():
    db(db.lassen_info.id == request.vars.lassen_info_id).delete()
    return "ok"


def get_losangeles_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    losangeles_info = []
    rows = db().select(db.losangeles_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            losangeles_info.append(t)
    return response.json(dict(
        losangeles_info=losangeles_info,
    ))


@auth.requires_signature()
def add_losangeles_info():
    i_id = db.losangeles_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(losangeles_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_losangeles_info():
    db(db.losangeles_info.id == request.vars.losangeles_info_id).delete()
    return "ok"


def get_madera_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    madera_info = []
    rows = db().select(db.madera_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            madera_info.append(t)
    return response.json(dict(
        madera_info=madera_info,
    ))


@auth.requires_signature()
def add_madera_info():
    i_id = db.madera_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(madera_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_madera_info():
    db(db.madera_info.id == request.vars.madera_info_id).delete()
    return "ok"


def get_marin_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    marin_info = []
    rows = db().select(db.marin_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            marin_info.append(t)
    return response.json(dict(
        marin_info=marin_info,
    ))


@auth.requires_signature()
def add_marin_info():
    i_id = db.marin_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(marin_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_marin_info():
    db(db.marin_info.id == request.vars.marin_info_id).delete()
    return "ok"


def get_mariposa_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    mariposa_info = []
    rows = db().select(db.mariposa_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            mariposa_info.append(t)
    return response.json(dict(
        mariposa_info=mariposa_info,
    ))


@auth.requires_signature()
def add_mariposa_info():
    i_id = db.mariposa_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(mariposa_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_mariposa_info():
    db(db.mariposa_info.id == request.vars.mariposa_info_id).delete()
    return "ok"


def get_mendocino_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    mendocino_info = []
    rows = db().select(db.mendocino_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            mendocino_info.append(t)
    return response.json(dict(
        mendocino_info=mendocino_info,
    ))


@auth.requires_signature()
def add_mendocino_info():
    i_id = db.mendocino_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(mendocino_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_mendocino_info():
    db(db.mendocino_info.id == request.vars.mendocino_info_id).delete()
    return "ok"


def get_merced_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    merced_info = []
    rows = db().select(db.merced_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            merced_info.append(t)
    return response.json(dict(
        merced_info=merced_info,
    ))


@auth.requires_signature()
def add_merced_info():
    i_id = db.merced_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(merced_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_merced_info():
    db(db.merced_info.id == request.vars.merced_info_id).delete()
    return "ok"


def get_modoc_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    modoc_info = []
    rows = db().select(db.modoc_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            modoc_info.append(t)
    return response.json(dict(
        modoc_info=modoc_info,
    ))


@auth.requires_signature()
def add_modoc_info():
    i_id = db.modoc_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(modoc_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_modoc_info():
    db(db.modoc_info.id == request.vars.modoc_info_id).delete()
    return "ok"


def get_mono_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    mono_info = []
    rows = db().select(db.mono_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            mono_info.append(t)
    return response.json(dict(
        mono_info=mono_info,
    ))


@auth.requires_signature()
def add_mono_info():
    i_id = db.mono_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(mono_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_mono_info():
    db(db.mono_info.id == request.vars.mono_info_id).delete()
    return "ok"


def get_monterey_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    monterey_info = []
    rows = db().select(db.monterey_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            monterey_info.append(t)
    return response.json(dict(
        monterey_info=monterey_info,
    ))


@auth.requires_signature()
def add_monterey_info():
    i_id = db.monterey_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(monterey_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_monterey_info():
    db(db.monterey_info.id == request.vars.monterey_info_id).delete()
    return "ok"


def get_napa_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    napa_info = []
    rows = db().select(db.napa_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            napa_info.append(t)
    return response.json(dict(
        napa_info=napa_info,
    ))


@auth.requires_signature()
def add_napa_info():
    i_id = db.napa_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(napa_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_napa_info():
    db(db.napa_info.id == request.vars.napa_info_id).delete()
    return "ok"


def get_nevada_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    nevada_info = []
    rows = db().select(db.nevada_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            nevada_info.append(t)
    return response.json(dict(
        nevada_info=nevada_info,
    ))


@auth.requires_signature()
def add_nevada_info():
    i_id = db.nevada_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(nevada_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_nevada_info():
    db(db.nevada_info.id == request.vars.nevada_info_id).delete()
    return "ok"


def get_orange_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    orange_info = []
    rows = db().select(db.orange_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            orange_info.append(t)
    return response.json(dict(
        orange_info=orange_info,
    ))


@auth.requires_signature()
def add_orange_info():
    i_id = db.orange_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(orange_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_orange_info():
    db(db.orange_info.id == request.vars.orange_info_id).delete()
    return "ok"


def get_placer_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    placer_info = []
    rows = db().select(db.placer_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            placer_info.append(t)
    return response.json(dict(
        placer_info=placer_info,
    ))


@auth.requires_signature()
def add_placer_info():
    i_id = db.placer_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(placer_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_placer_info():
    db(db.placer_info.id == request.vars.placer_info_id).delete()
    return "ok"


def get_plumas_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    plumas_info = []
    rows = db().select(db.plumas_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            plumas_info.append(t)
    return response.json(dict(
        plumas_info=plumas_info,
    ))


@auth.requires_signature()
def add_plumas_info():
    i_id = db.plumas_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(plumas_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_plumas_info():
    db(db.plumas_info.id == request.vars.plumas_info_id).delete()
    return "ok"


def get_riverside_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    riverside_info = []
    rows = db().select(db.riverside_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            riverside_info.append(t)
    return response.json(dict(
        riverside_info=riverside_info,
    ))


@auth.requires_signature()
def add_riverside_info():
    i_id = db.riverside_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(riverside_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_riverside_info():
    db(db.riverside_info.id == request.vars.riverside_info_id).delete()
    return "ok"


def get_sacramento_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    sacramento_info = []
    rows = db().select(db.sacramento_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            sacramento_info.append(t)
    return response.json(dict(
        sacramento_info=sacramento_info,
    ))


@auth.requires_signature()
def add_sacramento_info():
    i_id = db.sacramento_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(sacramento_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_sacramento_info():
    db(db.sacramento_info.id == request.vars.sacramento_info_id).delete()
    return "ok"


def get_sanbenito_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    sanbenito_info = []
    rows = db().select(db.sanbenito_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            sanbenito_info.append(t)
    return response.json(dict(
        sanbenito_info=sanbenito_info,
    ))


@auth.requires_signature()
def add_sanbenito_info():
    i_id = db.sanbenito_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(sanbenito_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_sanbenito_info():
    db(db.sanbenito_info.id == request.vars.sanbenito_info_id).delete()
    return "ok"


def get_sanbernardino_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    sanbernardino_info = []
    rows = db().select(db.sanbernardino_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            sanbernardino_info.append(t)
    return response.json(dict(
        sanbernardino_info=sanbernardino_info,
    ))


@auth.requires_signature()
def add_sanbernardino_info():
    i_id = db.sanbernardino_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(sanbernardino_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_sanbernardino_info():
    db(db.sanbernardino_info.id == request.vars.sanbernardino_info_id).delete()
    return "ok"


def get_sandiego_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    sandiego_info = []
    rows = db().select(db.sandiego_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            sandiego_info.append(t)
    return response.json(dict(
        sandiego_info=sandiego_info,
    ))


@auth.requires_signature()
def add_sandiego_info():
    i_id = db.sandiego_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(sandiego_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_sandiego_info():
    db(db.sandiego_info.id == request.vars.sandiego_info_id).delete()
    return "ok"


def get_sanfrancisco_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    sanfrancisco_info = []
    rows = db().select(db.sanfrancisco_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            sanfrancisco_info.append(t)
    return response.json(dict(
        sanfrancisco_info=sanfrancisco_info,
    ))


@auth.requires_signature()
def add_sanfrancisco_info():
    i_id = db.sanfrancisco_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(sanfrancisco_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_sanfrancisco_info():
    db(db.sanfrancisco_info.id == request.vars.sanfrancisco_info_id).delete()
    return "ok"


def get_sanjoaquin_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    sanjoaquin_info = []
    rows = db().select(db.sanjoaquin_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            sanjoaquin_info.append(t)
    return response.json(dict(
        sanjoaquin_info=sanjoaquin_info,
    ))


@auth.requires_signature()
def add_sanjoaquin_info():
    i_id = db.sanjoaquin_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(sanjoaquin_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_sanjoaquin_info():
    db(db.sanjoaquin_info.id == request.vars.sanjoaquin_info_id).delete()
    return "ok"


def get_sanluisobispo_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    sanluisobispo_info = []
    rows = db().select(db.sanluisobispo_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            sanluisobispo_info.append(t)
    return response.json(dict(
        sanluisobispo_info=sanluisobispo_info,
    ))


@auth.requires_signature()
def add_sanluisobispo_info():
    i_id = db.sanluisobispo_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(sanluisobispo_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_sanluisobispo_info():
    db(db.sanluisobispo_info.id == request.vars.sanluisobispo_info_id).delete()
    return "ok"


def get_sanmateo_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    sanmateo_info = []
    rows = db().select(db.sanmateo_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            sanmateo_info.append(t)
    return response.json(dict(
        sanmateo_info=sanmateo_info,
    ))


@auth.requires_signature()
def add_sanmateo_info():
    i_id = db.sanmateo_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(sanmateo_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_sanmateo_info():
    db(db.sanmateo_info.id == request.vars.sanmateo_info_id).delete()
    return "ok"


def get_santabarbara_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    santabarbara_info = []
    rows = db().select(db.santabarbara_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            santabarbara_info.append(t)
    return response.json(dict(
        santabarbara_info=santabarbara_info,
    ))


@auth.requires_signature()
def add_santabarbara_info():
    i_id = db.santabarbara_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(santabarbara_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_santabarbara_info():
    db(db.santabarbara_info.id == request.vars.santabarbara_info_id).delete()
    return "ok"


def get_santaclara_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    santaclara_info = []
    rows = db().select(db.santaclara_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            santaclara_info.append(t)
    return response.json(dict(
        santaclara_info=santaclara_info,
    ))


@auth.requires_signature()
def add_santaclara_info():
    i_id = db.santaclara_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(santaclara_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_santaclara_info():
    db(db.santaclara_info.id == request.vars.santaclara_info_id).delete()
    return "ok"


def get_santacruz_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    santacruz_info = []
    rows = db().select(db.santacruz_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            santacruz_info.append(t)
    return response.json(dict(
        santacruz_info=santacruz_info,
    ))


@auth.requires_signature()
def add_santacruz_info():
    i_id = db.santacruz_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(santacruz_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_santacruz_info():
    db(db.santacruz_info.id == request.vars.santacruz_info_id).delete()
    return "ok"


def get_shasta_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    shasta_info = []
    rows = db().select(db.shasta_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            shasta_info.append(t)
    return response.json(dict(
        shasta_info=shasta_info,
    ))


@auth.requires_signature()
def add_shasta_info():
    i_id = db.shasta_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(shasta_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_shasta_info():
    db(db.shasta_info.id == request.vars.shasta_info_id).delete()
    return "ok"


def get_sierra_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    sierra_info = []
    rows = db().select(db.sierra_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            sierra_info.append(t)
    return response.json(dict(
        sierra_info=sierra_info,
    ))


@auth.requires_signature()
def add_sierra_info():
    i_id = db.sierra_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(sierra_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_sierra_info():
    db(db.sierra_info.id == request.vars.sierra_info_id).delete()
    return "ok"


def get_siskiyou_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    siskiyou_info = []
    rows = db().select(db.siskiyou_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            siskiyou_info.append(t)
    return response.json(dict(
        siskiyou_info=siskiyou_info,
    ))


@auth.requires_signature()
def add_siskiyou_info():
    i_id = db.siskiyou_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(siskiyou_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_siskiyou_info():
    db(db.siskiyou_info.id == request.vars.siskiyou_info_id).delete()
    return "ok"


def get_solano_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    solano_info = []
    rows = db().select(db.solano_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            solano_info.append(t)
    return response.json(dict(
        solano_info=solano_info,
    ))


@auth.requires_signature()
def add_solano_info():
    i_id = db.solano_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(solano_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_solano_info():
    db(db.solano_info.id == request.vars.solano_info_id).delete()
    return "ok"


def get_sonoma_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    sonoma_info = []
    rows = db().select(db.sonoma_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            sonoma_info.append(t)
    return response.json(dict(
        sonoma_info=sonoma_info,
    ))


@auth.requires_signature()
def add_sonoma_info():
    i_id = db.sonoma_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(sonoma_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_sonoma_info():
    db(db.sonoma_info.id == request.vars.sonoma_info_id).delete()
    return "ok"


def get_stanislaus_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    stanislaus_info = []
    rows = db().select(db.stanislaus_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            stanislaus_info.append(t)
    return response.json(dict(
        stanislaus_info=stanislaus_info,
    ))


@auth.requires_signature()
def add_stanislaus_info():
    i_id = db.stanislaus_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(stanislaus_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_stanislaus_info():
    db(db.stanislaus_info.id == request.vars.stanislaus_info_id).delete()
    return "ok"


def get_sutter_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    sutter_info = []
    rows = db().select(db.sutter_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            sutter_info.append(t)
    return response.json(dict(
        sutter_info=sutter_info,
    ))


@auth.requires_signature()
def add_sutter_info():
    i_id = db.sutter_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(sutter_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_sutter_info():
    db(db.sutter_info.id == request.vars.sutter_info_id).delete()
    return "ok"


def get_tehama_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    tehama_info = []
    rows = db().select(db.tehama_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            tehama_info.append(t)
    return response.json(dict(
        tehama_info=tehama_info,
    ))


@auth.requires_signature()
def add_tehama_info():
    i_id = db.tehama_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(tehama_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_tehama_info():
    db(db.tehama_info.id == request.vars.tehama_info_id).delete()
    return "ok"


def get_trinity_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    trinity_info = []
    rows = db().select(db.trinity_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            trinity_info.append(t)
    return response.json(dict(
        trinity_info=trinity_info,
    ))


@auth.requires_signature()
def add_trinity_info():
    i_id = db.trinity_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(trinity_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_trinity_info():
    db(db.trinity_info.id == request.vars.trinity_info_id).delete()
    return "ok"


def get_tulare_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    tulare_info = []
    rows = db().select(db.tulare_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            tulare_info.append(t)
    return response.json(dict(
        tulare_info=tulare_info,
    ))


@auth.requires_signature()
def add_tulare_info():
    i_id = db.tulare_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(tulare_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_tulare_info():
    db(db.tulare_info.id == request.vars.tulare_info_id).delete()
    return "ok"


def get_tuolumne_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    tuolumne_info = []
    rows = db().select(db.tuolumne_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            tuolumne_info.append(t)
    return response.json(dict(
        tuolumne_info=tuolumne_info,
    ))


@auth.requires_signature()
def add_tuolumne_info():
    i_id = db.tuolumne_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(tuolumne_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_tuolumne_info():
    db(db.tuolumne_info.id == request.vars.tuolumne_info_id).delete()
    return "ok"


def get_ventura_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    ventura_info = []
    rows = db().select(db.ventura_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            ventura_info.append(t)
    return response.json(dict(
        ventura_info=ventura_info,
    ))


@auth.requires_signature()
def add_ventura_info():
    i_id = db.ventura_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(ventura_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_ventura_info():
    db(db.ventura_info.id == request.vars.ventura_info_id).delete()
    return "ok"


def get_yolo_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    yolo_info = []
    rows = db().select(db.yolo_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            yolo_info.append(t)
    return response.json(dict(
        yolo_info=yolo_info,
    ))


@auth.requires_signature()
def add_yolo_info():
    i_id = db.yolo_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(yolo_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_yolo_info():
    db(db.yolo_info.id == request.vars.yolo_info_id).delete()
    return "ok"


def get_yuba_info():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    yuba_info = []
    rows = db().select(db.yuba_info.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=r.id,
                artist=r.artist,
                city=r.city,
                url=r.url,
            )
            yuba_info.append(t)
    return response.json(dict(
        yuba_info=yuba_info,
    ))


@auth.requires_signature()
def add_yuba_info():
    i_id = db.yuba_info.insert(
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )
    return response.json(dict(yuba_info=dict(
        id=i_id,
        artist=request.vars.artist,
        city=request.vars.city,
        url=request.vars.url,
    )))


@auth.requires_signature()
def delete_yuba_info():
    db(db.yuba_info.id == request.vars.yuba_info_id).delete()
    return "ok"
