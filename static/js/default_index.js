// This is the js for the default/index.html view.

var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };

    var enumerate = function(v) { var k = 0; return v.map(function(e) {e._idx = k++;});};

    self.insertion_id = null;

    //-------------Alameda------------------------------

    function get_alameda_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return alameda_info_url + "?" + $.param(pp);
    }



    self.get_alameda_info = function() {
        $.getJSON(get_alameda_info_url(0, 100), function (data) {
            self.vue.alameda_info = data.alameda_info;
            enumerate(self.vue.alameda_info);
        })
    };

    self.add_alameda_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_alameda_info = true;
    };

    self.cancel_alameda_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_alameda_info = false;
    };

    self.add_alameda_info = function () {
        $.post(add_alameda_info_url,
            {
                artist: self.vue.form_alameda_artist,
                city: self.vue.form_alameda_city,
                url: self.vue.form_alameda_url
            },
            function (data) {
            $.web2py.enableElement($("#add_alameda_info_submit"));
            self.vue.is_adding_alameda_info = false;
            self.vue.alameda_info.unshift(data.alameda_info);
            enumerate(self.vue.alameda_info);
            self.vue.form_alameda_artist = "";
            self.vue.form_alameda_city = "";
            self.vue.form_alameda_url = "";
            });
    };

    self.delete_alameda_info = function(info_idx) {
        $.post(del_alameda_info_url,
            {
                alameda_info_id: self.vue.alameda_info[info_idx].id
            },
            function () {
                self.vue.alameda_info.splice(info_idx, 1);
                enumerate(self.vue.alameda_info);
            })
    };


    function get_alpine_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
    return alpine_info_url + "?" + $.param(pp);
    }

    self.get_alpine_info = function() {
        $.getJSON(get_alpine_info_url(0, 100), function (data) {
            self.vue.alpine_info = data.alpine_info;
            enumerate(self.vue.alpine_info);
        })
    };

    self.add_alpine_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_alpine_info = true;
    };

    self.cancel_alpine_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_alpine_info = false;
    };

    self.add_alpine_info = function () {
        $.post(add_alpine_info_url,
            {
                artist: self.vue.form_alpine_artist,
                city: self.vue.form_alpine_city,
                url: self.vue.form_alpine_url
            },
            function (data) {
            $.web2py.enableElement($("#add_alpine_info_submit"));
            self.vue.is_adding_alpine_info = false;
            self.vue.alpine_info.unshift(data.alpine_info);
            enumerate(self.vue.alpine_info);
            self.vue.form_alpine_artist = "";
            self.vue.form_alpine_city = "";
            self.vue.form_alpine_url = "";
            });
    };

    self.delete_alpine_info = function(info_idx) {
        $.post(del_alpine_info_url,
            {
                alpine_info_id: self.vue.alpine_info[info_idx].id
            },
            function () {
                self.vue.alpine_info.splice(info_idx, 1);
                enumerate(self.vue.alpine_info);
            })
    };


    //-------------------AMador-------------------------
    function get_amador_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return amador_info_url + "?" + $.param(pp);
    }

    self.get_amador_info = function() {
        $.getJSON(get_amador_info_url(0, 100), function (data) {
            self.vue.amador_info = data.amador_info;
            enumerate(self.vue.amador_info);
        })
    };

    self.add_amador_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_amador_info = true;
    };

    self.cancel_amador_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_amador_info = false;
    };

    self.add_amador_info = function () {
        $.post(add_amador_info_url,
            {
                artist: self.vue.form_amador_artist,
                city: self.vue.form_amador_city,
					 url: self.vue.form_amador_url
            },
            function (data) {
            $.web2py.enableElement($("#add_amador_info_submit"));
            self.vue.is_adding_amador_info = false;
            self.vue.amador_info.unshift(data.amador_info);
            enumerate(self.vue.amador_info);
            self.vue.form_amador_artist = "";
            self.vue.form_amador_city = "";
				self.vue.form_amador_url = "";
            });
    };

    self.delete_amador_info = function(info_idx) {
        $.post(del_amador_info_url,
            {
                amador_info_id: self.vue.amador_info[info_idx].id
            },
            function () {
                self.vue.amador_info.splice(info_idx, 1);
                enumerate(self.vue.amador_info);
            })
    };


    //--------------------Butte---------------------
    function get_butte_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return butte_info_url + "?" + $.param(pp);
    }

    self.get_butte_info = function() {
        $.getJSON(get_butte_info_url(0, 100), function (data) {
            self.vue.butte_info = data.butte_info;
            enumerate(self.vue.butte_info);
        })
    };

    self.add_butte_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_butte_info = true;
    };

    self.cancel_butte_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_butte_info = false;
    };

    self.add_butte_info = function () {
        $.post(add_butte_info_url,
            {
                artist: self.vue.form_butte_artist,
                city: self.vue.form_butte_city,
					 url: self.vue.form_butte_url
            },
            function (data) {
            $.web2py.enableElement($("#add_butte_info_submit"));
            self.vue.is_adding_butte_info = false;
            self.vue.butte_info.unshift(data.butte_info);
            enumerate(self.vue.butte_info);
            self.vue.form_butte_artist = "";
            self.vue.form_butte_city = "";
				self.vue.form_butte_url = "";
            });
    };

    self.delete_butte_info = function(info_idx) {
        $.post(del_butte_info_url,
            {
                butte_info_id: self.vue.butte_info[info_idx].id
            },
            function () {
                self.vue.butte_info.splice(info_idx, 1);
                enumerate(self.vue.butte_info);
            })
    };

    //---------------------Calaveras--------------
    function get_calaveras_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return calaveras_info_url + "?" + $.param(pp);
    }

    self.get_calaveras_info = function() {
        $.getJSON(get_calaveras_info_url(0, 100), function (data) {
            self.vue.calaveras_info = data.calaveras_info;
            enumerate(self.vue.calaveras_info);
        })
    };

    self.add_calaveras_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_calaveras_info = true;
    };

    self.cancel_calaveras_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_calaveras_info = false;
    };

    self.add_calaveras_info = function () {
        $.post(add_calaveras_info_url,
            {
                artist: self.vue.form_calaveras_artist,
                city: self.vue.form_calaveras_city,
					 url: self.vue.form_calaveras_url
            },
            function (data) {
            $.web2py.enableElement($("#add_calaveras_info_submit"));
            self.vue.is_adding_calaveras_info = false;
            self.vue.calaveras_info.unshift(data.calaveras_info);
            enumerate(self.vue.calaveras_info);
            self.vue.form_calaveras_artist = "";
            self.vue.form_calaveras_city = "";
				self.vue.form_calaveras_url = "";
            });
    };

    self.delete_calaveras_info = function(info_idx) {
        $.post(del_calaveras_info_url,
            {
                calaveras_info_id: self.vue.calaveras_info[info_idx].id
            },
            function () {
                self.vue.calaveras_info.splice(info_idx, 1);
                enumerate(self.vue.calaveras_info);
            })
    };

    //====================Colusa---------------------------
    function get_colusa_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return colusa_info_url + "?" + $.param(pp);
    }

    self.get_colusa_info = function() {
        $.getJSON(get_colusa_info_url(0, 100), function (data) {
            self.vue.colusa_info = data.colusa_info;
            enumerate(self.vue.colusa_info);
        })
    };

    self.add_colusa_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_colusa_info = true;
    };

    self.cancel_colusa_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_colusa_info = false;
    };

    self.add_colusa_info = function () {
        $.post(add_colusa_info_url,
            {
                artist: self.vue.form_colusa_artist,
                city: self.vue.form_colusa_city,
					 url: self.vue.form_colusa_url
            },
            function (data) {
            $.web2py.enableElement($("#add_colusa_info_submit"));
            self.vue.is_adding_colusa_info = false;
            self.vue.colusa_info.unshift(data.colusa_info);
            enumerate(self.vue.colusa_info);
            self.vue.form_colusa_artist = "";
            self.vue.form_colusa_city = "";
				self.vue.form_colusa_url = "";
            });
    };

    self.delete_colusa_info = function(info_idx) {
        $.post(del_colusa_info_url,
            {
                colusa_info_id: self.vue.colusa_info[info_idx].id
            },
            function () {
                self.vue.colusa_info.splice(info_idx, 1);
                enumerate(self.vue.colusa_info);
            })
    };


    //--------------------------Contra Costa
    function get_contracosta_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return contracosta_info_url + "?" + $.param(pp);
    }

    self.get_contracosta_info = function() {
        $.getJSON(get_contracosta_info_url(0, 100), function (data) {
            self.vue.contracosta_info = data.contracosta_info;
            enumerate(self.vue.contracosta_info);
        })
    };

    self.add_contracosta_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_contracosta_info = true;
    };

    self.cancel_contracosta_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_contracosta_info = false;
    };

    self.add_contracosta_info = function () {
        $.post(add_contracosta_info_url,
            {
                artist: self.vue.form_contracosta_artist,
                city: self.vue.form_contracosta_city,
					 url: self.vue.form_contracosta_url
            },
            function (data) {
            $.web2py.enableElement($("#add_contracosta_info_submit"));
            self.vue.is_adding_contracosta_info = false;
            self.vue.contracosta_info.unshift(data.contracosta_info);
            enumerate(self.vue.contracosta_info);
            self.vue.form_contracosta_artist = "";
            self.vue.form_contracosta_city = "";
				self.vue.form_contracosta_url = "";
            });
    };

    self.delete_contracosta_info = function(info_idx) {
        $.post(del_contracosta_info_url,
            {
                contracosta_info_id: self.vue.contracosta_info[info_idx].id
            },
            function () {
                self.vue.contracosta_info.splice(info_idx, 1);
                enumerate(self.vue.contracosta_info);
            })
    };


    //-------------Del Norte-----------
    function get_delnorte_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return delnorte_info_url + "?" + $.param(pp);
    }

    self.get_delnorte_info = function() {
        $.getJSON(get_delnorte_info_url(0, 100), function (data) {
            self.vue.delnorte_info = data.delnorte_info;
            enumerate(self.vue.delnorte_info);
        })
    };

    self.add_delnorte_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_delnorte_info = true;
    };

    self.cancel_delnorte_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_delnorte_info = false;
    };

    self.add_delnorte_info = function () {
        $.post(add_delnorte_info_url,
            {
                artist: self.vue.form_delnorte_artist,
                city: self.vue.form_delnorte_city,
					 url: self.vue.form_delnorte_url
            },
            function (data) {
            $.web2py.enableElement($("#add_delnorte_info_submit"));
            self.vue.is_adding_delnorte_info = false;
            self.vue.delnorte_info.unshift(data.delnorte_info);
            enumerate(self.vue.delnorte_info);
            self.vue.form_delnorte_artist = "";
            self.vue.form_delnorte_city = "";
				self.vue.form_delnorte_url = "";
            });
    };

    self.delete_delnorte_info = function(info_idx) {
        $.post(del_delnorte_info_url,
            {
                delnorte_info_id: self.vue.delnorte_info[info_idx].id
            },
            function () {
                self.vue.delnorte_info.splice(info_idx, 1);
                enumerate(self.vue.delnorte_info);
            })
    };


    //-----------------EL DOrado---------------
    function get_eldorado_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return eldorado_info_url + "?" + $.param(pp);
    }

    self.get_eldorado_info = function() {
        $.getJSON(get_eldorado_info_url(0, 100), function (data) {
            self.vue.eldorado_info = data.eldorado_info;
            enumerate(self.vue.eldorado_info);
        })
    };

    self.add_eldorado_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_eldorado_info = true;
    };

    self.cancel_eldorado_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_eldorado_info = false;
    };

    self.add_eldorado_info = function () {
        $.post(add_eldorado_info_url,
            {
                artist: self.vue.form_eldorado_artist,
                city: self.vue.form_eldorado_city,
					 url: self.vue.form_eldorado_url
            },
            function (data) {
            $.web2py.enableElement($("#add_eldorado_info_submit"));
            self.vue.is_adding_eldorado_info = false;
            self.vue.eldorado_info.unshift(data.eldorado_info);
            enumerate(self.vue.eldorado_info);
            self.vue.form_eldorado_artist = "";
            self.vue.form_eldorado_city = "";
				self.vue.form_eldorado_url = "";
            });
    };

    self.delete_eldorado_info = function(info_idx) {
        $.post(del_eldorado_info_url,
            {
                eldorado_info_id: self.vue.eldorado_info[info_idx].id
            },
            function () {
                self.vue.eldorado_info.splice(info_idx, 1);
                enumerate(self.vue.eldorado_info);
            })
    };


    //------------------Fresno------------------
    function get_fresno_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return fresno_info_url + "?" + $.param(pp);
    }

    self.get_fresno_info = function() {
        $.getJSON(get_fresno_info_url(0, 100), function (data) {
            self.vue.fresno_info = data.fresno_info;
            enumerate(self.vue.fresno_info);
        })
    };

    self.add_fresno_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_fresno_info = true;
    };

    self.cancel_fresno_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_fresno_info = false;
    };

    self.add_fresno_info = function () {
        $.post(add_fresno_info_url,
            {
                artist: self.vue.form_fresno_artist,
                city: self.vue.form_fresno_city,
					 url: self.vue.form_fresno_url
            },
            function (data) {
            $.web2py.enableElement($("#add_fresno_info_submit"));
            self.vue.is_adding_fresno_info = false;
            self.vue.fresno_info.unshift(data.fresno_info);
            enumerate(self.vue.fresno_info);
            self.vue.form_fresno_artist = "";
            self.vue.form_fresno_city = "";
				self.vue.form_fresno_url = "";
            });
    };

    self.delete_fresno_info = function(info_idx) {
        $.post(del_fresno_info_url,
            {
                fresno_info_id: self.vue.fresno_info[info_idx].id
            },
            function () {
                self.vue.fresno_info.splice(info_idx, 1);
                enumerate(self.vue.fresno_info);
            })
    };

    //-------------glenn------------------------------

    function get_glenn_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return glenn_info_url + "?" + $.param(pp);
    }



    self.get_glenn_info = function() {
        $.getJSON(get_glenn_info_url(0, 100), function (data) {
            self.vue.glenn_info = data.glenn_info;
            enumerate(self.vue.glenn_info);
        })
    };

    self.add_glenn_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_glenn_info = true;
    };

    self.cancel_glenn_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_glenn_info = false;
    };

    self.add_glenn_info = function () {
        $.post(add_glenn_info_url,
            {
                artist: self.vue.form_glenn_artist,
                city: self.vue.form_glenn_city,
					 url: self.vue.form_glenn_url
            },
            function (data) {
            $.web2py.enableElement($("#add_glenn_info_submit"));
            self.vue.is_adding_glenn_info = false;
            self.vue.glenn_info.unshift(data.glenn_info);
            enumerate(self.vue.glenn_info);
            self.vue.form_glenn_artist = "";
            self.vue.form_glenn_city = "";
				self.vue.form_glenn_url = "";
            });
    };

    self.delete_glenn_info = function(info_idx) {
        $.post(del_glenn_info_url,
            {
                glenn_info_id: self.vue.glenn_info[info_idx].id
            },
            function () {
                self.vue.glenn_info.splice(info_idx, 1);
                enumerate(self.vue.glenn_info);
            })
    };


    //------------------------humboldt----------------
	 function get_humboldt_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
    return humboldt_info_url + "?" + $.param(pp);
    }

    self.get_humboldt_info = function() {
        $.getJSON(get_humboldt_info_url(0, 100), function (data) {
            self.vue.humboldt_info = data.humboldt_info;
            enumerate(self.vue.humboldt_info);
        })
    };

    self.add_humboldt_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_humboldt_info = true;
    };

    self.cancel_humboldt_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_humboldt_info = false;
    };

    self.add_humboldt_info = function () {
        $.post(add_humboldt_info_url,
            {
                artist: self.vue.form_humboldt_artist,
                city: self.vue.form_humboldt_city,
                url: self.vue.form_humboldt_url
            },
            function (data) {
            $.web2py.enableElement($("#add_humboldt_info_submit"));
            self.vue.is_adding_humboldt_info = false;
            self.vue.humboldt_info.unshift(data.humboldt_info);
            enumerate(self.vue.humboldt_info);
            self.vue.form_humboldt_artist = "";
            self.vue.form_humboldt_city = "";
            self.vue.form_humboldt_url = "";
            });
    };

    self.delete_humboldt_info = function(info_idx) {
        $.post(del_humboldt_info_url,
            {
                humboldt_info_id: self.vue.humboldt_info[info_idx].id
            },
            function () {
                self.vue.humboldt_info.splice(info_idx, 1);
                enumerate(self.vue.humboldt_info);
            })
    };


    //------------------imperial-------------------------
    function get_imperial_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return imperial_info_url + "?" + $.param(pp);
    }

    self.get_imperial_info = function() {
        $.getJSON(get_imperial_info_url(0, 100), function (data) {
            self.vue.imperial_info = data.imperial_info;
            enumerate(self.vue.imperial_info);
        })
    };

    self.add_imperial_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_imperial_info = true;
    };

    self.cancel_imperial_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_imperial_info = false;
    };

    self.add_imperial_info = function () {
        $.post(add_imperial_info_url,
            {
                artist: self.vue.form_imperial_artist,
                city: self.vue.form_imperial_city,
					 url: self.vue.form_imperial_url
            },
            function (data) {
            $.web2py.enableElement($("#add_imperial_info_submit"));
            self.vue.is_adding_imperial_info = false;
            self.vue.imperial_info.unshift(data.imperial_info);
            enumerate(self.vue.imperial_info);
            self.vue.form_imperial_artist = "";
            self.vue.form_imperial_city = "";
				self.vue.form_imperial_url = "";
            });
    };

    self.delete_imperial_info = function(info_idx) {
        $.post(del_imperial_info_url,
            {
                imperial_info_id: self.vue.imperial_info[info_idx].id
            },
            function () {
                self.vue.imperial_info.splice(info_idx, 1);
                enumerate(self.vue.imperial_info);
            })
    };


    //--------------------inyo---------------------
    function get_inyo_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return inyo_info_url + "?" + $.param(pp);
    }

    self.get_inyo_info = function() {
        $.getJSON(get_inyo_info_url(0, 100), function (data) {
            self.vue.inyo_info = data.inyo_info;
            enumerate(self.vue.inyo_info);
        })
    };

    self.add_inyo_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_inyo_info = true;
    };

    self.cancel_inyo_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_inyo_info = false;
    };

    self.add_inyo_info = function () {
        $.post(add_inyo_info_url,
            {
                artist: self.vue.form_inyo_artist,
                city: self.vue.form_inyo_city,
					 url: self.vue.form_inyo_url
            },
            function (data) {
            $.web2py.enableElement($("#add_inyo_info_submit"));
            self.vue.is_adding_inyo_info = false;
            self.vue.inyo_info.unshift(data.inyo_info);
            enumerate(self.vue.inyo_info);
            self.vue.form_inyo_artist = "";
            self.vue.form_inyo_city = "";
				self.vue.form_inyo_url = "";
            });
    };

    self.delete_inyo_info = function(info_idx) {
        $.post(del_inyo_info_url,
            {
                inyo_info_id: self.vue.inyo_info[info_idx].id
            },
            function () {
                self.vue.inyo_info.splice(info_idx, 1);
                enumerate(self.vue.inyo_info);
            })
    };

    //---------------------kern--------------
    function get_kern_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return kern_info_url + "?" + $.param(pp);
    }

    self.get_kern_info = function() {
        $.getJSON(get_kern_info_url(0, 100), function (data) {
            self.vue.kern_info = data.kern_info;
            enumerate(self.vue.kern_info);
        })
    };

    self.add_kern_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_kern_info = true;
    };

    self.cancel_kern_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_kern_info = false;
    };

    self.add_kern_info = function () {
        $.post(add_kern_info_url,
            {
                artist: self.vue.form_kern_artist,
                city: self.vue.form_kern_city,
					 url: self.vue.form_kern_url
            },
            function (data) {
            $.web2py.enableElement($("#add_kern_info_submit"));
            self.vue.is_adding_kern_info = false;
            self.vue.kern_info.unshift(data.kern_info);
            enumerate(self.vue.kern_info);
            self.vue.form_kern_artist = "";
            self.vue.form_kern_city = "";
				self.vue.form_kern_url = "";
            });
    };

    self.delete_kern_info = function(info_idx) {
        $.post(del_kern_info_url,
            {
                kern_info_id: self.vue.kern_info[info_idx].id
            },
            function () {
                self.vue.kern_info.splice(info_idx, 1);
                enumerate(self.vue.kern_info);
            })
    };

    //====================kings---------------------------
    function get_kings_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return kings_info_url + "?" + $.param(pp);
    }

    self.get_kings_info = function() {
        $.getJSON(get_kings_info_url(0, 100), function (data) {
            self.vue.kings_info = data.kings_info;
            enumerate(self.vue.kings_info);
        })
    };

    self.add_kings_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_kings_info = true;
    };

    self.cancel_kings_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_kings_info = false;
    };

    self.add_kings_info = function () {
        $.post(add_kings_info_url,
            {
                artist: self.vue.form_kings_artist,
                city: self.vue.form_kings_city,
					 url: self.vue.form_kings_url
            },
            function (data) {
            $.web2py.enableElement($("#add_kings_info_submit"));
            self.vue.is_adding_kings_info = false;
            self.vue.kings_info.unshift(data.kings_info);
            enumerate(self.vue.kings_info);
            self.vue.form_kings_artist = "";
            self.vue.form_kings_city = "";
				self.vue.form_kings_url = "";
            });
    };

    self.delete_kings_info = function(info_idx) {
        $.post(del_kings_info_url,
            {
                kings_info_id: self.vue.kings_info[info_idx].id
            },
            function () {
                self.vue.kings_info.splice(info_idx, 1);
                enumerate(self.vue.kings_info);
            })
    };


    //--------------------------lake
    function get_lake_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return lake_info_url + "?" + $.param(pp);
    }

    self.get_lake_info = function() {
        $.getJSON(get_lake_info_url(0, 100), function (data) {
            self.vue.lake_info = data.lake_info;
            enumerate(self.vue.lake_info);
        })
    };

    self.add_lake_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_lake_info = true;
    };

    self.cancel_lake_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_lake_info = false;
    };

    self.add_lake_info = function () {
        $.post(add_lake_info_url,
            {
                artist: self.vue.form_lake_artist,
                city: self.vue.form_lake_city,
					 url: self.vue.form_lake_url
            },
            function (data) {
            $.web2py.enableElement($("#add_lake_info_submit"));
            self.vue.is_adding_lake_info = false;
            self.vue.lake_info.unshift(data.lake_info);
            enumerate(self.vue.lake_info);
            self.vue.form_lake_artist = "";
            self.vue.form_lake_city = "";
				self.vue.form_lake_url = "";
            });
    };

    self.delete_lake_info = function(info_idx) {
        $.post(del_lake_info_url,
            {
                lake_info_id: self.vue.lake_info[info_idx].id
            },
            function () {
                self.vue.lake_info.splice(info_idx, 1);
                enumerate(self.vue.lake_info);
            })
    };


    //-------------lassen-----------
    function get_lassen_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return lassen_info_url + "?" + $.param(pp);
    }

    self.get_lassen_info = function() {
        $.getJSON(get_lassen_info_url(0, 100), function (data) {
            self.vue.lassen_info = data.lassen_info;
            enumerate(self.vue.lassen_info);
        })
    };

    self.add_lassen_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_lassen_info = true;
    };

    self.cancel_lassen_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_lassen_info = false;
    };

    self.add_lassen_info = function () {
        $.post(add_lassen_info_url,
            {
                artist: self.vue.form_lassen_artist,
                city: self.vue.form_lassen_city,
					 url: self.vue.form_lassen_url
            },
            function (data) {
            $.web2py.enableElement($("#add_lassen_info_submit"));
            self.vue.is_adding_lassen_info = false;
            self.vue.lassen_info.unshift(data.lassen_info);
            enumerate(self.vue.lassen_info);
            self.vue.form_lassen_artist = "";
            self.vue.form_lassen_city = "";
				self.vue.form_lassen_url = "";
            });
    };

    self.delete_lassen_info = function(info_idx) {
        $.post(del_lassen_info_url,
            {
                lassen_info_id: self.vue.lassen_info[info_idx].id
            },
            function () {
                self.vue.lassen_info.splice(info_idx, 1);
                enumerate(self.vue.lassen_info);
            })
    };


    //-----------------losangeles---------------
    function get_losangeles_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return losangeles_info_url + "?" + $.param(pp);
    }

    self.get_losangeles_info = function() {
        $.getJSON(get_losangeles_info_url(0, 100), function (data) {
            self.vue.losangeles_info = data.losangeles_info;
            enumerate(self.vue.losangeles_info);
        })
    };

    self.add_losangeles_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_losangeles_info = true;
    };

    self.cancel_losangeles_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_losangeles_info = false;
    };

    self.add_losangeles_info = function () {
        $.post(add_losangeles_info_url,
            {
                artist: self.vue.form_losangeles_artist,
                city: self.vue.form_losangeles_city,
					 url: self.vue.form_losangeles_url
            },
            function (data) {
            $.web2py.enableElement($("#add_losangeles_info_submit"));
            self.vue.is_adding_losangeles_info = false;
            self.vue.losangeles_info.unshift(data.losangeles_info);
            enumerate(self.vue.losangeles_info);
            self.vue.form_losangeles_artist = "";
            self.vue.form_losangeles_city = "";
				self.vue.form_losangeles_url = "";
            });
    };

    self.delete_losangeles_info = function(info_idx) {
        $.post(del_losangeles_info_url,
            {
                losangeles_info_id: self.vue.losangeles_info[info_idx].id
            },
            function () {
                self.vue.losangeles_info.splice(info_idx, 1);
                enumerate(self.vue.losangeles_info);
            })
    };


    //------------------madera------------------
    function get_madera_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return madera_info_url + "?" + $.param(pp);
    }

    self.get_madera_info = function() {
        $.getJSON(get_madera_info_url(0, 100), function (data) {
            self.vue.madera_info = data.madera_info;
            enumerate(self.vue.madera_info);
        })
    };

    self.add_madera_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_madera_info = true;
    };

    self.cancel_madera_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_madera_info = false;
    };

    self.add_madera_info = function () {
        $.post(add_madera_info_url,
            {
                artist: self.vue.form_madera_artist,
                city: self.vue.form_madera_city,
					 url: self.vue.form_madera_url
            },
            function (data) {
            $.web2py.enableElement($("#add_madera_info_submit"));
            self.vue.is_adding_madera_info = false;
            self.vue.madera_info.unshift(data.madera_info);
            enumerate(self.vue.madera_info);
            self.vue.form_madera_artist = "";
            self.vue.form_madera_city = "";
				self.vue.form_madera_url = "";
            });
    };

    self.delete_madera_info = function(info_idx) {
        $.post(del_madera_info_url,
            {
                madera_info_id: self.vue.madera_info[info_idx].id
            },
            function () {
                self.vue.madera_info.splice(info_idx, 1);
                enumerate(self.vue.madera_info);
            })
    };


    //-------------marin------------------------------

    function get_marin_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return marin_info_url + "?" + $.param(pp);
    }



    self.get_marin_info = function() {
        $.getJSON(get_marin_info_url(0, 100), function (data) {
            self.vue.marin_info = data.marin_info;
            enumerate(self.vue.marin_info);
        })
    };

    self.add_marin_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_marin_info = true;
    };

    self.cancel_marin_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_marin_info = false;
    };

    self.add_marin_info = function () {
        $.post(add_marin_info_url,
            {
                artist: self.vue.form_marin_artist,
                city: self.vue.form_marin_city,
					 url: self.vue.form_marin_url
            },
            function (data) {
            $.web2py.enableElement($("#add_marin_info_submit"));
            self.vue.is_adding_marin_info = false;
            self.vue.marin_info.unshift(data.marin_info);
            enumerate(self.vue.marin_info);
            self.vue.form_marin_artist = "";
            self.vue.form_marin_city = "";
				self.vue.form_marin_url = "";
            });
    };

    self.delete_marin_info = function(info_idx) {
        $.post(del_marin_info_url,
            {
                marin_info_id: self.vue.marin_info[info_idx].id
            },
            function () {
                self.vue.marin_info.splice(info_idx, 1);
                enumerate(self.vue.marin_info);
            })
    };


    //------------------------mariposa----------------
	 function get_mariposa_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
    return mariposa_info_url + "?" + $.param(pp);
    }

    self.get_mariposa_info = function() {
        $.getJSON(get_mariposa_info_url(0, 100), function (data) {
            self.vue.mariposa_info = data.mariposa_info;
            enumerate(self.vue.mariposa_info);
        })
    };

    self.add_mariposa_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_mariposa_info = true;
    };

    self.cancel_mariposa_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_mariposa_info = false;
    };

    self.add_mariposa_info = function () {
        $.post(add_mariposa_info_url,
            {
                artist: self.vue.form_mariposa_artist,
                city: self.vue.form_mariposa_city,
                url: self.vue.form_mariposa_url
            },
            function (data) {
            $.web2py.enableElement($("#add_mariposa_info_submit"));
            self.vue.is_adding_mariposa_info = false;
            self.vue.mariposa_info.unshift(data.mariposa_info);
            enumerate(self.vue.mariposa_info);
            self.vue.form_mariposa_artist = "";
            self.vue.form_mariposa_city = "";
            self.vue.form_mariposa_url = "";
            });
    };

    self.delete_mariposa_info = function(info_idx) {
        $.post(del_mariposa_info_url,
            {
                mariposa_info_id: self.vue.mariposa_info[info_idx].id
            },
            function () {
                self.vue.mariposa_info.splice(info_idx, 1);
                enumerate(self.vue.mariposa_info);
            })
    };


    //------------------mendocino-------------------------
    function get_mendocino_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return mendocino_info_url + "?" + $.param(pp);
    }

    self.get_mendocino_info = function() {
        $.getJSON(get_mendocino_info_url(0, 100), function (data) {
            self.vue.mendocino_info = data.mendocino_info;
            enumerate(self.vue.mendocino_info);
        })
    };

    self.add_mendocino_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_mendocino_info = true;
    };

    self.cancel_mendocino_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_mendocino_info = false;
    };

    self.add_mendocino_info = function () {
        $.post(add_mendocino_info_url,
            {
                artist: self.vue.form_mendocino_artist,
                city: self.vue.form_mendocino_city,
					 url: self.vue.form_mendocino_url
            },
            function (data) {
            $.web2py.enableElement($("#add_mendocino_info_submit"));
            self.vue.is_adding_mendocino_info = false;
            self.vue.mendocino_info.unshift(data.mendocino_info);
            enumerate(self.vue.mendocino_info);
            self.vue.form_mendocino_artist = "";
            self.vue.form_mendocino_city = "";
				self.vue.form_mendocino_url = "";
            });
    };

    self.delete_mendocino_info = function(info_idx) {
        $.post(del_mendocino_info_url,
            {
                mendocino_info_id: self.vue.mendocino_info[info_idx].id
            },
            function () {
                self.vue.mendocino_info.splice(info_idx, 1);
                enumerate(self.vue.mendocino_info);
            })
    };


    //--------------------merced---------------------
    function get_merced_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return merced_info_url + "?" + $.param(pp);
    }

    self.get_merced_info = function() {
        $.getJSON(get_merced_info_url(0, 100), function (data) {
            self.vue.merced_info = data.merced_info;
            enumerate(self.vue.merced_info);
        })
    };

    self.add_merced_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_merced_info = true;
    };

    self.cancel_merced_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_merced_info = false;
    };

    self.add_merced_info = function () {
        $.post(add_merced_info_url,
            {
                artist: self.vue.form_merced_artist,
                city: self.vue.form_merced_city,
					 url: self.vue.form_merced_url
            },
            function (data) {
            $.web2py.enableElement($("#add_merced_info_submit"));
            self.vue.is_adding_merced_info = false;
            self.vue.merced_info.unshift(data.merced_info);
            enumerate(self.vue.merced_info);
            self.vue.form_merced_artist = "";
            self.vue.form_merced_city = "";
				self.vue.form_merced_url = "";
            });
    };

    self.delete_merced_info = function(info_idx) {
        $.post(del_merced_info_url,
            {
                merced_info_id: self.vue.merced_info[info_idx].id
            },
            function () {
                self.vue.merced_info.splice(info_idx, 1);
                enumerate(self.vue.merced_info);
            })
    };

    //---------------------modoc--------------
    function get_modoc_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return modoc_info_url + "?" + $.param(pp);
    }

    self.get_modoc_info = function() {
        $.getJSON(get_modoc_info_url(0, 100), function (data) {
            self.vue.modoc_info = data.modoc_info;
            enumerate(self.vue.modoc_info);
        })
    };

    self.add_modoc_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_modoc_info = true;
    };

    self.cancel_modoc_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_modoc_info = false;
    };

    self.add_modoc_info = function () {
        $.post(add_modoc_info_url,
            {
                artist: self.vue.form_modoc_artist,
                city: self.vue.form_modoc_city,
					 url: self.vue.form_modoc_url
            },
            function (data) {
            $.web2py.enableElement($("#add_modoc_info_submit"));
            self.vue.is_adding_modoc_info = false;
            self.vue.modoc_info.unshift(data.modoc_info);
            enumerate(self.vue.modoc_info);
            self.vue.form_modoc_artist = "";
            self.vue.form_modoc_city = "";
				self.vue.form_modoc_url = "";
            });
    };

    self.delete_modoc_info = function(info_idx) {
        $.post(del_modoc_info_url,
            {
                modoc_info_id: self.vue.modoc_info[info_idx].id
            },
            function () {
                self.vue.modoc_info.splice(info_idx, 1);
                enumerate(self.vue.modoc_info);
            })
    };

    //====================mono---------------------------
    function get_mono_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return mono_info_url + "?" + $.param(pp);
    }

    self.get_mono_info = function() {
        $.getJSON(get_mono_info_url(0, 100), function (data) {
            self.vue.mono_info = data.mono_info;
            enumerate(self.vue.mono_info);
        })
    };

    self.add_mono_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_mono_info = true;
    };

    self.cancel_mono_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_mono_info = false;
    };

    self.add_mono_info = function () {
        $.post(add_mono_info_url,
            {
                artist: self.vue.form_mono_artist,
                city: self.vue.form_mono_city,
					 url: self.vue.form_mono_url
            },
            function (data) {
            $.web2py.enableElement($("#add_mono_info_submit"));
            self.vue.is_adding_mono_info = false;
            self.vue.mono_info.unshift(data.mono_info);
            enumerate(self.vue.mono_info);
            self.vue.form_mono_artist = "";
            self.vue.form_mono_city = "";
				self.vue.form_mono_url = "";
            });
    };

    self.delete_mono_info = function(info_idx) {
        $.post(del_mono_info_url,
            {
                mono_info_id: self.vue.mono_info[info_idx].id
            },
            function () {
                self.vue.mono_info.splice(info_idx, 1);
                enumerate(self.vue.mono_info);
            })
    };


    //--------------------------monterey
    function get_monterey_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return monterey_info_url + "?" + $.param(pp);
    }

    self.get_monterey_info = function() {
        $.getJSON(get_monterey_info_url(0, 100), function (data) {
            self.vue.monterey_info = data.monterey_info;
            enumerate(self.vue.monterey_info);
        })
    };

    self.add_monterey_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_monterey_info = true;
    };

    self.cancel_monterey_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_monterey_info = false;
    };

    self.add_monterey_info = function () {
        $.post(add_monterey_info_url,
            {
                artist: self.vue.form_monterey_artist,
                city: self.vue.form_monterey_city,
					 url: self.vue.form_monterey_url
            },
            function (data) {
            $.web2py.enableElement($("#add_monterey_info_submit"));
            self.vue.is_adding_monterey_info = false;
            self.vue.monterey_info.unshift(data.monterey_info);
            enumerate(self.vue.monterey_info);
            self.vue.form_monterey_artist = "";
            self.vue.form_monterey_city = "";
				self.vue.form_monterey_url = "";
            });
    };

    self.delete_monterey_info = function(info_idx) {
        $.post(del_monterey_info_url,
            {
                monterey_info_id: self.vue.monterey_info[info_idx].id
            },
            function () {
                self.vue.monterey_info.splice(info_idx, 1);
                enumerate(self.vue.monterey_info);
            })
    };


    //-------------napa-----------
    function get_napa_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return napa_info_url + "?" + $.param(pp);
    }

    self.get_napa_info = function() {
        $.getJSON(get_napa_info_url(0, 100), function (data) {
            self.vue.napa_info = data.napa_info;
            enumerate(self.vue.napa_info);
        })
    };

    self.add_napa_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_napa_info = true;
    };

    self.cancel_napa_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_napa_info = false;
    };

    self.add_napa_info = function () {
        $.post(add_napa_info_url,
            {
                artist: self.vue.form_napa_artist,
                city: self.vue.form_napa_city,
					 url: self.vue.form_napa_url
            },
            function (data) {
            $.web2py.enableElement($("#add_napa_info_submit"));
            self.vue.is_adding_napa_info = false;
            self.vue.napa_info.unshift(data.napa_info);
            enumerate(self.vue.napa_info);
            self.vue.form_napa_artist = "";
            self.vue.form_napa_city = "";
				self.vue.form_napa_url = "";
            });
    };

    self.delete_napa_info = function(info_idx) {
        $.post(del_napa_info_url,
            {
                napa_info_id: self.vue.napa_info[info_idx].id
            },
            function () {
                self.vue.napa_info.splice(info_idx, 1);
                enumerate(self.vue.napa_info);
            })
    };


    //-----------------nevada---------------
    function get_nevada_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return nevada_info_url + "?" + $.param(pp);
    }

    self.get_nevada_info = function() {
        $.getJSON(get_nevada_info_url(0, 100), function (data) {
            self.vue.nevada_info = data.nevada_info;
            enumerate(self.vue.nevada_info);
        })
    };

    self.add_nevada_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_nevada_info = true;
    };

    self.cancel_nevada_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_nevada_info = false;
    };

    self.add_nevada_info = function () {
        $.post(add_nevada_info_url,
            {
                artist: self.vue.form_nevada_artist,
                city: self.vue.form_nevada_city,
					 url: self.vue.form_nevada_url
            },
            function (data) {
            $.web2py.enableElement($("#add_nevada_info_submit"));
            self.vue.is_adding_nevada_info = false;
            self.vue.nevada_info.unshift(data.nevada_info);
            enumerate(self.vue.nevada_info);
            self.vue.form_nevada_artist = "";
            self.vue.form_nevada_city = "";
				self.vue.form_nevada_url = "";
            });
    };

    self.delete_nevada_info = function(info_idx) {
        $.post(del_nevada_info_url,
            {
                nevada_info_id: self.vue.nevada_info[info_idx].id
            },
            function () {
                self.vue.nevada_info.splice(info_idx, 1);
                enumerate(self.vue.nevada_info);
            })
    };


    //------------------orange------------------
    function get_orange_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return orange_info_url + "?" + $.param(pp);
    }

    self.get_orange_info = function() {
        $.getJSON(get_orange_info_url(0, 100), function (data) {
            self.vue.orange_info = data.orange_info;
            enumerate(self.vue.orange_info);
        })
    };

    self.add_orange_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_orange_info = true;
    };

    self.cancel_orange_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_orange_info = false;
    };

    self.add_orange_info = function () {
        $.post(add_orange_info_url,
            {
                artist: self.vue.form_orange_artist,
                city: self.vue.form_orange_city,
					 url: self.vue.form_orange_url
            },
            function (data) {
            $.web2py.enableElement($("#add_orange_info_submit"));
            self.vue.is_adding_orange_info = false;
            self.vue.orange_info.unshift(data.orange_info);
            enumerate(self.vue.orange_info);
            self.vue.form_orange_artist = "";
            self.vue.form_orange_city = "";
				self.vue.form_orange_url = "";
            });
    };

    self.delete_orange_info = function(info_idx) {
        $.post(del_orange_info_url,
            {
                orange_info_id: self.vue.orange_info[info_idx].id
            },
            function () {
                self.vue.orange_info.splice(info_idx, 1);
                enumerate(self.vue.orange_info);
            })
    };

    //-------------placer------------------------------

    function get_placer_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return placer_info_url + "?" + $.param(pp);
    }



    self.get_placer_info = function() {
        $.getJSON(get_placer_info_url(0, 100), function (data) {
            self.vue.placer_info = data.placer_info;
            enumerate(self.vue.placer_info);
        })
    };

    self.add_placer_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_placer_info = true;
    };

    self.cancel_placer_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_placer_info = false;
    };

    self.add_placer_info = function () {
        $.post(add_placer_info_url,
            {
                artist: self.vue.form_placer_artist,
                city: self.vue.form_placer_city,
					 url: self.vue.form_placer_url
            },
            function (data) {
            $.web2py.enableElement($("#add_placer_info_submit"));
            self.vue.is_adding_placer_info = false;
            self.vue.placer_info.unshift(data.placer_info);
            enumerate(self.vue.placer_info);
            self.vue.form_placer_artist = "";
            self.vue.form_placer_city = "";
				self.vue.form_placer_url = "";
            });
    };

    self.delete_placer_info = function(info_idx) {
        $.post(del_placer_info_url,
            {
                placer_info_id: self.vue.placer_info[info_idx].id
            },
            function () {
                self.vue.placer_info.splice(info_idx, 1);
                enumerate(self.vue.placer_info);
            })
    };


    //------------------------plumas----------------
	 function get_plumas_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
    return plumas_info_url + "?" + $.param(pp);
    }

    self.get_plumas_info = function() {
        $.getJSON(get_plumas_info_url(0, 100), function (data) {
            self.vue.plumas_info = data.plumas_info;
            enumerate(self.vue.plumas_info);
        })
    };

    self.add_plumas_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_plumas_info = true;
    };

    self.cancel_plumas_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_plumas_info = false;
    };

    self.add_plumas_info = function () {
        $.post(add_plumas_info_url,
            {
                artist: self.vue.form_plumas_artist,
                city: self.vue.form_plumas_city,
                url: self.vue.form_plumas_url
            },
            function (data) {
            $.web2py.enableElement($("#add_plumas_info_submit"));
            self.vue.is_adding_plumas_info = false;
            self.vue.plumas_info.unshift(data.plumas_info);
            enumerate(self.vue.plumas_info);
            self.vue.form_plumas_artist = "";
            self.vue.form_plumas_city = "";
            self.vue.form_plumas_url = "";
            });
    };

    self.delete_plumas_info = function(info_idx) {
        $.post(del_plumas_info_url,
            {
                plumas_info_id: self.vue.plumas_info[info_idx].id
            },
            function () {
                self.vue.plumas_info.splice(info_idx, 1);
                enumerate(self.vue.plumas_info);
            })
    };


    //------------------riverside-------------------------
    function get_riverside_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return riverside_info_url + "?" + $.param(pp);
    }

    self.get_riverside_info = function() {
        $.getJSON(get_riverside_info_url(0, 100), function (data) {
            self.vue.riverside_info = data.riverside_info;
            enumerate(self.vue.riverside_info);
        })
    };

    self.add_riverside_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_riverside_info = true;
    };

    self.cancel_riverside_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_riverside_info = false;
    };

    self.add_riverside_info = function () {
        $.post(add_riverside_info_url,
            {
                artist: self.vue.form_riverside_artist,
                city: self.vue.form_riverside_city,
					 url: self.vue.form_riverside_url
            },
            function (data) {
            $.web2py.enableElement($("#add_riverside_info_submit"));
            self.vue.is_adding_riverside_info = false;
            self.vue.riverside_info.unshift(data.riverside_info);
            enumerate(self.vue.riverside_info);
            self.vue.form_riverside_artist = "";
            self.vue.form_riverside_city = "";
				self.vue.form_riverside_url = "";
            });
    };

    self.delete_riverside_info = function(info_idx) {
        $.post(del_riverside_info_url,
            {
                riverside_info_id: self.vue.riverside_info[info_idx].id
            },
            function () {
                self.vue.riverside_info.splice(info_idx, 1);
                enumerate(self.vue.riverside_info);
            })
    };


    //--------------------sacramento---------------------
    function get_sacramento_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return sacramento_info_url + "?" + $.param(pp);
    }

    self.get_sacramento_info = function() {
        $.getJSON(get_sacramento_info_url(0, 100), function (data) {
            self.vue.sacramento_info = data.sacramento_info;
            enumerate(self.vue.sacramento_info);
        })
    };

    self.add_sacramento_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_sacramento_info = true;
    };

    self.cancel_sacramento_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_sacramento_info = false;
    };

    self.add_sacramento_info = function () {
        $.post(add_sacramento_info_url,
            {
                artist: self.vue.form_sacramento_artist,
                city: self.vue.form_sacramento_city,
					 url: self.vue.form_sacramento_url
            },
            function (data) {
            $.web2py.enableElement($("#add_sacramento_info_submit"));
            self.vue.is_adding_sacramento_info = false;
            self.vue.sacramento_info.unshift(data.sacramento_info);
            enumerate(self.vue.sacramento_info);
            self.vue.form_sacramento_artist = "";
            self.vue.form_sacramento_city = "";
				self.vue.form_sacramento_url = "";
            });
    };

    self.delete_sacramento_info = function(info_idx) {
        $.post(del_sacramento_info_url,
            {
                sacramento_info_id: self.vue.sacramento_info[info_idx].id
            },
            function () {
                self.vue.sacramento_info.splice(info_idx, 1);
                enumerate(self.vue.sacramento_info);
            })
    };

    //---------------------sanbenito--------------
    function get_sanbenito_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return sanbenito_info_url + "?" + $.param(pp);
    }

    self.get_sanbenito_info = function() {
        $.getJSON(get_sanbenito_info_url(0, 100), function (data) {
            self.vue.sanbenito_info = data.sanbenito_info;
            enumerate(self.vue.sanbenito_info);
        })
    };

    self.add_sanbenito_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_sanbenito_info = true;
    };

    self.cancel_sanbenito_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_sanbenito_info = false;
    };

    self.add_sanbenito_info = function () {
        $.post(add_sanbenito_info_url,
            {
                artist: self.vue.form_sanbenito_artist,
                city: self.vue.form_sanbenito_city,
					 url: self.vue.form_sanbenito_url
            },
            function (data) {
            $.web2py.enableElement($("#add_sanbenito_info_submit"));
            self.vue.is_adding_sanbenito_info = false;
            self.vue.sanbenito_info.unshift(data.sanbenito_info);
            enumerate(self.vue.sanbenito_info);
            self.vue.form_sanbenito_artist = "";
            self.vue.form_sanbenito_city = "";
				self.vue.form_sanbenito_url = "";
            });
    };

    self.delete_sanbenito_info = function(info_idx) {
        $.post(del_sanbenito_info_url,
            {
                sanbenito_info_id: self.vue.sanbenito_info[info_idx].id
            },
            function () {
                self.vue.sanbenito_info.splice(info_idx, 1);
                enumerate(self.vue.sanbenito_info);
            })
    };

    //====================sanbernardino---------------------------
    function get_sanbernardino_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return sanbernardino_info_url + "?" + $.param(pp);
    }

    self.get_sanbernardino_info = function() {
        $.getJSON(get_sanbernardino_info_url(0, 100), function (data) {
            self.vue.sanbernardino_info = data.sanbernardino_info;
            enumerate(self.vue.sanbernardino_info);
        })
    };

    self.add_sanbernardino_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_sanbernardino_info = true;
    };

    self.cancel_sanbernardino_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_sanbernardino_info = false;
    };

    self.add_sanbernardino_info = function () {
        $.post(add_sanbernardino_info_url,
            {
                artist: self.vue.form_sanbernardino_artist,
                city: self.vue.form_sanbernardino_city,
					 url: self.vue.form_sanbernardino_url
            },
            function (data) {
            $.web2py.enableElement($("#add_sanbernardino_info_submit"));
            self.vue.is_adding_sanbernardino_info = false;
            self.vue.sanbernardino_info.unshift(data.sanbernardino_info);
            enumerate(self.vue.sanbernardino_info);
            self.vue.form_sanbernardino_artist = "";
            self.vue.form_sanbernardino_city = "";
				self.vue.form_sanbernardino_url = "";
            });
    };

    self.delete_sanbernardino_info = function(info_idx) {
        $.post(del_sanbernardino_info_url,
            {
                sanbernardino_info_id: self.vue.sanbernardino_info[info_idx].id
            },
            function () {
                self.vue.sanbernardino_info.splice(info_idx, 1);
                enumerate(self.vue.sanbernardino_info);
            })
    };


    //--------------------------sandiego
    function get_sandiego_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return sandiego_info_url + "?" + $.param(pp);
    }

    self.get_sandiego_info = function() {
        $.getJSON(get_sandiego_info_url(0, 100), function (data) {
            self.vue.sandiego_info = data.sandiego_info;
            enumerate(self.vue.sandiego_info);
        })
    };

    self.add_sandiego_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_sandiego_info = true;
    };

    self.cancel_sandiego_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_sandiego_info = false;
    };

    self.add_sandiego_info = function () {
        $.post(add_sandiego_info_url,
            {
                artist: self.vue.form_sandiego_artist,
                city: self.vue.form_sandiego_city,
					 url: self.vue.form_sandiego_url
            },
            function (data) {
            $.web2py.enableElement($("#add_sandiego_info_submit"));
            self.vue.is_adding_sandiego_info = false;
            self.vue.sandiego_info.unshift(data.sandiego_info);
            enumerate(self.vue.sandiego_info);
            self.vue.form_sandiego_artist = "";
            self.vue.form_sandiego_city = "";
				self.vue.form_sandiego_url = "";
            });
    };

    self.delete_sandiego_info = function(info_idx) {
        $.post(del_sandiego_info_url,
            {
                sandiego_info_id: self.vue.sandiego_info[info_idx].id
            },
            function () {
                self.vue.sandiego_info.splice(info_idx, 1);
                enumerate(self.vue.sandiego_info);
            })
    };


    //-------------sanfrancisco-----------
    function get_sanfrancisco_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return sanfrancisco_info_url + "?" + $.param(pp);
    }

    self.get_sanfrancisco_info = function() {
        $.getJSON(get_sanfrancisco_info_url(0, 100), function (data) {
            self.vue.sanfrancisco_info = data.sanfrancisco_info;
            enumerate(self.vue.sanfrancisco_info);
        })
    };

    self.add_sanfrancisco_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_sanfrancisco_info = true;
    };

    self.cancel_sanfrancisco_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_sanfrancisco_info = false;
    };

    self.add_sanfrancisco_info = function () {
        $.post(add_sanfrancisco_info_url,
            {
                artist: self.vue.form_sanfrancisco_artist,
                city: self.vue.form_sanfrancisco_city,
					 url: self.vue.form_sanfrancisco_url
            },
            function (data) {
            $.web2py.enableElement($("#add_sanfrancisco_info_submit"));
            self.vue.is_adding_sanfrancisco_info = false;
            self.vue.sanfrancisco_info.unshift(data.sanfrancisco_info);
            enumerate(self.vue.sanfrancisco_info);
            self.vue.form_sanfrancisco_artist = "";
            self.vue.form_sanfrancisco_city = "";
				self.vue.form_sanfrancisco_url = "";
            });
    };

    self.delete_sanfrancisco_info = function(info_idx) {
        $.post(del_sanfrancisco_info_url,
            {
                sanfrancisco_info_id: self.vue.sanfrancisco_info[info_idx].id
            },
            function () {
                self.vue.sanfrancisco_info.splice(info_idx, 1);
                enumerate(self.vue.sanfrancisco_info);
            })
    };


    //-----------------sanjoaquin---------------
    function get_sanjoaquin_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return sanjoaquin_info_url + "?" + $.param(pp);
    }

    self.get_sanjoaquin_info = function() {
        $.getJSON(get_sanjoaquin_info_url(0, 100), function (data) {
            self.vue.sanjoaquin_info = data.sanjoaquin_info;
            enumerate(self.vue.sanjoaquin_info);
        })
    };

    self.add_sanjoaquin_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_sanjoaquin_info = true;
    };

    self.cancel_sanjoaquin_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_sanjoaquin_info = false;
    };

    self.add_sanjoaquin_info = function () {
        $.post(add_sanjoaquin_info_url,
            {
                artist: self.vue.form_sanjoaquin_artist,
                city: self.vue.form_sanjoaquin_city,
					 url: self.vue.form_sanjoaquin_url
            },
            function (data) {
            $.web2py.enableElement($("#add_sanjoaquin_info_submit"));
            self.vue.is_adding_sanjoaquin_info = false;
            self.vue.sanjoaquin_info.unshift(data.sanjoaquin_info);
            enumerate(self.vue.sanjoaquin_info);
            self.vue.form_sanjoaquin_artist = "";
            self.vue.form_sanjoaquin_city = "";
				self.vue.form_sanjoaquin_url = "";
            });
    };

    self.delete_sanjoaquin_info = function(info_idx) {
        $.post(del_sanjoaquin_info_url,
            {
                sanjoaquin_info_id: self.vue.sanjoaquin_info[info_idx].id
            },
            function () {
                self.vue.sanjoaquin_info.splice(info_idx, 1);
                enumerate(self.vue.sanjoaquin_info);
            })
    };


    //------------------sanluisobispo------------------
    function get_sanluisobispo_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return sanluisobispo_info_url + "?" + $.param(pp);
    }

    self.get_sanluisobispo_info = function() {
        $.getJSON(get_sanluisobispo_info_url(0, 100), function (data) {
            self.vue.sanluisobispo_info = data.sanluisobispo_info;
            enumerate(self.vue.sanluisobispo_info);
        })
    };

    self.add_sanluisobispo_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_sanluisobispo_info = true;
    };

    self.cancel_sanluisobispo_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_sanluisobispo_info = false;
    };

    self.add_sanluisobispo_info = function () {
        $.post(add_sanluisobispo_info_url,
            {
                artist: self.vue.form_sanluisobispo_artist,
                city: self.vue.form_sanluisobispo_city,
					 url: self.vue.form_sanluisobispo_url
            },
            function (data) {
            $.web2py.enableElement($("#add_sanluisobispo_info_submit"));
            self.vue.is_adding_sanluisobispo_info = false;
            self.vue.sanluisobispo_info.unshift(data.sanluisobispo_info);
            enumerate(self.vue.sanluisobispo_info);
            self.vue.form_sanluisobispo_artist = "";
            self.vue.form_sanluisobispo_city = "";
				self.vue.form_sanluisobispo_url = "";
            });
    };

    self.delete_sanluisobispo_info = function(info_idx) {
        $.post(del_sanluisobispo_info_url,
            {
                sanluisobispo_info_id: self.vue.sanluisobispo_info[info_idx].id
            },
            function () {
                self.vue.sanluisobispo_info.splice(info_idx, 1);
                enumerate(self.vue.sanluisobispo_info);
            })
    };

    //-------------sanmateo------------------------------

    function get_sanmateo_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return sanmateo_info_url + "?" + $.param(pp);
    }



    self.get_sanmateo_info = function() {
        $.getJSON(get_sanmateo_info_url(0, 100), function (data) {
            self.vue.sanmateo_info = data.sanmateo_info;
            enumerate(self.vue.sanmateo_info);
        })
    };

    self.add_sanmateo_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_sanmateo_info = true;
    };

    self.cancel_sanmateo_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_sanmateo_info = false;
    };

    self.add_sanmateo_info = function () {
        $.post(add_sanmateo_info_url,
            {
                artist: self.vue.form_sanmateo_artist,
                city: self.vue.form_sanmateo_city,
					 url: self.vue.form_sanmateo_url
            },
            function (data) {
            $.web2py.enableElement($("#add_sanmateo_info_submit"));
            self.vue.is_adding_sanmateo_info = false;
            self.vue.sanmateo_info.unshift(data.sanmateo_info);
            enumerate(self.vue.sanmateo_info);
            self.vue.form_sanmateo_artist = "";
            self.vue.form_sanmateo_city = "";
				self.vue.form_sanmateo_url = "";
            });
    };

    self.delete_sanmateo_info = function(info_idx) {
        $.post(del_sanmateo_info_url,
            {
                sanmateo_info_id: self.vue.sanmateo_info[info_idx].id
            },
            function () {
                self.vue.sanmateo_info.splice(info_idx, 1);
                enumerate(self.vue.sanmateo_info);
            })
    };


    //------------------------santabarbara----------------
	 function get_santabarbara_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
    return santabarbara_info_url + "?" + $.param(pp);
    }

    self.get_santabarbara_info = function() {
        $.getJSON(get_santabarbara_info_url(0, 100), function (data) {
            self.vue.santabarbara_info = data.santabarbara_info;
            enumerate(self.vue.santabarbara_info);
        })
    };

    self.add_santabarbara_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_santabarbara_info = true;
    };

    self.cancel_santabarbara_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_santabarbara_info = false;
    };

    self.add_santabarbara_info = function () {
        $.post(add_santabarbara_info_url,
            {
                artist: self.vue.form_santabarbara_artist,
                city: self.vue.form_santabarbara_city,
                url: self.vue.form_santabarbara_url
            },
            function (data) {
            $.web2py.enableElement($("#add_santabarbara_info_submit"));
            self.vue.is_adding_santabarbara_info = false;
            self.vue.santabarbara_info.unshift(data.santabarbara_info);
            enumerate(self.vue.santabarbara_info);
            self.vue.form_santabarbara_artist = "";
            self.vue.form_santabarbara_city = "";
            self.vue.form_santabarbara_url = "";
            });
    };

    self.delete_santabarbara_info = function(info_idx) {
        $.post(del_santabarbara_info_url,
            {
                santabarbara_info_id: self.vue.santabarbara_info[info_idx].id
            },
            function () {
                self.vue.santabarbara_info.splice(info_idx, 1);
                enumerate(self.vue.santabarbara_info);
            })
    };


    //------------------santaclara-------------------------
    function get_santaclara_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return santaclara_info_url + "?" + $.param(pp);
    }

    self.get_santaclara_info = function() {
        $.getJSON(get_santaclara_info_url(0, 100), function (data) {
            self.vue.santaclara_info = data.santaclara_info;
            enumerate(self.vue.santaclara_info);
        })
    };

    self.add_santaclara_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_santaclara_info = true;
    };

    self.cancel_santaclara_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_santaclara_info = false;
    };

    self.add_santaclara_info = function () {
        $.post(add_santaclara_info_url,
            {
                artist: self.vue.form_santaclara_artist,
                city: self.vue.form_santaclara_city,
					 url: self.vue.form_santaclara_url
            },
            function (data) {
            $.web2py.enableElement($("#add_santaclara_info_submit"));
            self.vue.is_adding_santaclara_info = false;
            self.vue.santaclara_info.unshift(data.santaclara_info);
            enumerate(self.vue.santaclara_info);
            self.vue.form_santaclara_artist = "";
            self.vue.form_santaclara_city = "";
				self.vue.form_santaclara_url = "";
            });
    };

    self.delete_santaclara_info = function(info_idx) {
        $.post(del_santaclara_info_url,
            {
                santaclara_info_id: self.vue.santaclara_info[info_idx].id
            },
            function () {
                self.vue.santaclara_info.splice(info_idx, 1);
                enumerate(self.vue.santaclara_info);
            })
    };


    //--------------------santacruz---------------------
    function get_santacruz_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return santacruz_info_url + "?" + $.param(pp);
    }

    self.get_santacruz_info = function() {
        $.getJSON(get_santacruz_info_url(0, 100), function (data) {
            self.vue.santacruz_info = data.santacruz_info;
            enumerate(self.vue.santacruz_info);
        })
    };

    self.add_santacruz_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_santacruz_info = true;
    };

    self.cancel_santacruz_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_santacruz_info = false;
    };

    self.add_santacruz_info = function () {
        $.post(add_santacruz_info_url,
            {
                artist: self.vue.form_santacruz_artist,
                city: self.vue.form_santacruz_city,
					 url: self.vue.form_santacruz_url
            },
            function (data) {
            $.web2py.enableElement($("#add_santacruz_info_submit"));
            self.vue.is_adding_santacruz_info = false;
            self.vue.santacruz_info.unshift(data.santacruz_info);
            enumerate(self.vue.santacruz_info);
            self.vue.form_santacruz_artist = "";
            self.vue.form_santacruz_city = "";
				self.vue.form_santacruz_url = "";
            });
    };

    self.delete_santacruz_info = function(info_idx) {
        $.post(del_santacruz_info_url,
            {
                santacruz_info_id: self.vue.santacruz_info[info_idx].id
            },
            function () {
                self.vue.santacruz_info.splice(info_idx, 1);
                enumerate(self.vue.santacruz_info);
            })
    };

    //---------------------shasta--------------
    function get_shasta_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return shasta_info_url + "?" + $.param(pp);
    }

    self.get_shasta_info = function() {
        $.getJSON(get_shasta_info_url(0, 100), function (data) {
            self.vue.shasta_info = data.shasta_info;
            enumerate(self.vue.shasta_info);
        })
    };

    self.add_shasta_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_shasta_info = true;
    };

    self.cancel_shasta_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_shasta_info = false;
    };

    self.add_shasta_info = function () {
        $.post(add_shasta_info_url,
            {
                artist: self.vue.form_shasta_artist,
                city: self.vue.form_shasta_city,
					 url: self.vue.form_shasta_url
            },
            function (data) {
            $.web2py.enableElement($("#add_shasta_info_submit"));
            self.vue.is_adding_shasta_info = false;
            self.vue.shasta_info.unshift(data.shasta_info);
            enumerate(self.vue.shasta_info);
            self.vue.form_shasta_artist = "";
            self.vue.form_shasta_city = "";
				self.vue.form_shasta_url = "";
            });
    };

    self.delete_shasta_info = function(info_idx) {
        $.post(del_shasta_info_url,
            {
                shasta_info_id: self.vue.shasta_info[info_idx].id
            },
            function () {
                self.vue.shasta_info.splice(info_idx, 1);
                enumerate(self.vue.shasta_info);
            })
    };

    //====================sierra---------------------------
    function get_sierra_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return sierra_info_url + "?" + $.param(pp);
    }

    self.get_sierra_info = function() {
        $.getJSON(get_sierra_info_url(0, 100), function (data) {
            self.vue.sierra_info = data.sierra_info;
            enumerate(self.vue.sierra_info);
        })
    };

    self.add_sierra_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_sierra_info = true;
    };

    self.cancel_sierra_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_sierra_info = false;
    };

    self.add_sierra_info = function () {
        $.post(add_sierra_info_url,
            {
                artist: self.vue.form_sierra_artist,
                city: self.vue.form_sierra_city,
					 url: self.vue.form_sierra_url
            },
            function (data) {
            $.web2py.enableElement($("#add_sierra_info_submit"));
            self.vue.is_adding_sierra_info = false;
            self.vue.sierra_info.unshift(data.sierra_info);
            enumerate(self.vue.sierra_info);
            self.vue.form_sierra_artist = "";
            self.vue.form_sierra_city = "";
				self.vue.form_sierra_url = "";
            });
    };

    self.delete_sierra_info = function(info_idx) {
        $.post(del_sierra_info_url,
            {
                sierra_info_id: self.vue.sierra_info[info_idx].id
            },
            function () {
                self.vue.sierra_info.splice(info_idx, 1);
                enumerate(self.vue.sierra_info);
            })
    };


    //--------------------------siskiyou
    function get_siskiyou_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return siskiyou_info_url + "?" + $.param(pp);
    }

    self.get_siskiyou_info = function() {
        $.getJSON(get_siskiyou_info_url(0, 100), function (data) {
            self.vue.siskiyou_info = data.siskiyou_info;
            enumerate(self.vue.siskiyou_info);
        })
    };

    self.add_siskiyou_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_siskiyou_info = true;
    };

    self.cancel_siskiyou_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_siskiyou_info = false;
    };

    self.add_siskiyou_info = function () {
        $.post(add_siskiyou_info_url,
            {
                artist: self.vue.form_siskiyou_artist,
                city: self.vue.form_siskiyou_city,
					 url: self.vue.form_siskiyou_url
            },
            function (data) {
            $.web2py.enableElement($("#add_siskiyou_info_submit"));
            self.vue.is_adding_siskiyou_info = false;
            self.vue.siskiyou_info.unshift(data.siskiyou_info);
            enumerate(self.vue.siskiyou_info);
            self.vue.form_siskiyou_artist = "";
            self.vue.form_siskiyou_city = "";
				self.vue.form_siskiyou_url = "";
            });
    };

    self.delete_siskiyou_info = function(info_idx) {
        $.post(del_siskiyou_info_url,
            {
                siskiyou_info_id: self.vue.siskiyou_info[info_idx].id
            },
            function () {
                self.vue.siskiyou_info.splice(info_idx, 1);
                enumerate(self.vue.siskiyou_info);
            })
    };


    //-------------solano-----------
    function get_solano_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return solano_info_url + "?" + $.param(pp);
    }

    self.get_solano_info = function() {
        $.getJSON(get_solano_info_url(0, 100), function (data) {
            self.vue.solano_info = data.solano_info;
            enumerate(self.vue.solano_info);
        })
    };

    self.add_solano_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_solano_info = true;
    };

    self.cancel_solano_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_solano_info = false;
    };

    self.add_solano_info = function () {
        $.post(add_solano_info_url,
            {
                artist: self.vue.form_solano_artist,
                city: self.vue.form_solano_city,
					 url: self.vue.form_solano_url
            },
            function (data) {
            $.web2py.enableElement($("#add_solano_info_submit"));
            self.vue.is_adding_solano_info = false;
            self.vue.solano_info.unshift(data.solano_info);
            enumerate(self.vue.solano_info);
            self.vue.form_solano_artist = "";
            self.vue.form_solano_city = "";
				self.vue.form_solano_url = "";
            });
    };

    self.delete_solano_info = function(info_idx) {
        $.post(del_solano_info_url,
            {
                solano_info_id: self.vue.solano_info[info_idx].id
            },
            function () {
                self.vue.solano_info.splice(info_idx, 1);
                enumerate(self.vue.solano_info);
            })
    };


    //-----------------sonoma---------------
    function get_sonoma_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return sonoma_info_url + "?" + $.param(pp);
    }

    self.get_sonoma_info = function() {
        $.getJSON(get_sonoma_info_url(0, 100), function (data) {
            self.vue.sonoma_info = data.sonoma_info;
            enumerate(self.vue.sonoma_info);
        })
    };

    self.add_sonoma_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_sonoma_info = true;
    };

    self.cancel_sonoma_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_sonoma_info = false;
    };

    self.add_sonoma_info = function () {
        $.post(add_sonoma_info_url,
            {
                artist: self.vue.form_sonoma_artist,
                city: self.vue.form_sonoma_city,
					 url: self.vue.form_sonoma_url
            },
            function (data) {
            $.web2py.enableElement($("#add_sonoma_info_submit"));
            self.vue.is_adding_sonoma_info = false;
            self.vue.sonoma_info.unshift(data.sonoma_info);
            enumerate(self.vue.sonoma_info);
            self.vue.form_sonoma_artist = "";
            self.vue.form_sonoma_city = "";
				self.vue.form_sonoma_url = "";
            });
    };

    self.delete_sonoma_info = function(info_idx) {
        $.post(del_sonoma_info_url,
            {
                sonoma_info_id: self.vue.sonoma_info[info_idx].id
            },
            function () {
                self.vue.sonoma_info.splice(info_idx, 1);
                enumerate(self.vue.sonoma_info);
            })
    };


    //------------------stanislaus------------------
    function get_stanislaus_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return stanislaus_info_url + "?" + $.param(pp);
    }

    self.get_stanislaus_info = function() {
        $.getJSON(get_stanislaus_info_url(0, 100), function (data) {
            self.vue.stanislaus_info = data.stanislaus_info;
            enumerate(self.vue.stanislaus_info);
        })
    };

    self.add_stanislaus_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_stanislaus_info = true;
    };

    self.cancel_stanislaus_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_stanislaus_info = false;
    };

    self.add_stanislaus_info = function () {
        $.post(add_stanislaus_info_url,
            {
                artist: self.vue.form_stanislaus_artist,
                city: self.vue.form_stanislaus_city,
					 url: self.vue.form_stanislaus_url
            },
            function (data) {
            $.web2py.enableElement($("#add_stanislaus_info_submit"));
            self.vue.is_adding_stanislaus_info = false;
            self.vue.stanislaus_info.unshift(data.stanislaus_info);
            enumerate(self.vue.stanislaus_info);
            self.vue.form_stanislaus_artist = "";
            self.vue.form_stanislaus_city = "";
				self.vue.form_stanislaus_url = "";
            });
    };

    self.delete_stanislaus_info = function(info_idx) {
        $.post(del_stanislaus_info_url,
            {
                stanislaus_info_id: self.vue.stanislaus_info[info_idx].id
            },
            function () {
                self.vue.stanislaus_info.splice(info_idx, 1);
                enumerate(self.vue.stanislaus_info);
            })
    };

    //-------------sutter------------------------------

    function get_sutter_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return sutter_info_url + "?" + $.param(pp);
    }



    self.get_sutter_info = function() {
        $.getJSON(get_sutter_info_url(0, 100), function (data) {
            self.vue.sutter_info = data.sutter_info;
            enumerate(self.vue.sutter_info);
        })
    };

    self.add_sutter_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_sutter_info = true;
    };

    self.cancel_sutter_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_sutter_info = false;
    };

    self.add_sutter_info = function () {
        $.post(add_sutter_info_url,
            {
                artist: self.vue.form_sutter_artist,
                city: self.vue.form_sutter_city,
					 url: self.vue.form_sutter_url
            },
            function (data) {
            $.web2py.enableElement($("#add_sutter_info_submit"));
            self.vue.is_adding_sutter_info = false;
            self.vue.sutter_info.unshift(data.sutter_info);
            enumerate(self.vue.sutter_info);
            self.vue.form_sutter_artist = "";
            self.vue.form_sutter_city = "";
				self.vue.form_sutter_url = "";
            });
    };

    self.delete_sutter_info = function(info_idx) {
        $.post(del_sutter_info_url,
            {
                sutter_info_id: self.vue.sutter_info[info_idx].id
            },
            function () {
                self.vue.sutter_info.splice(info_idx, 1);
                enumerate(self.vue.sutter_info);
            })
    };


    //------------------------tehama----------------
	 function get_tehama_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
    return tehama_info_url + "?" + $.param(pp);
    }

    self.get_tehama_info = function() {
        $.getJSON(get_tehama_info_url(0, 100), function (data) {
            self.vue.tehama_info = data.tehama_info;
            enumerate(self.vue.tehama_info);
        })
    };

    self.add_tehama_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_tehama_info = true;
    };

    self.cancel_tehama_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_tehama_info = false;
    };

    self.add_tehama_info = function () {
        $.post(add_tehama_info_url,
            {
                artist: self.vue.form_tehama_artist,
                city: self.vue.form_tehama_city,
                url: self.vue.form_tehama_url
            },
            function (data) {
            $.web2py.enableElement($("#add_tehama_info_submit"));
            self.vue.is_adding_tehama_info = false;
            self.vue.tehama_info.unshift(data.tehama_info);
            enumerate(self.vue.tehama_info);
            self.vue.form_tehama_artist = "";
            self.vue.form_tehama_city = "";
            self.vue.form_tehama_url = "";
            });
    };

    self.delete_tehama_info = function(info_idx) {
        $.post(del_tehama_info_url,
            {
                tehama_info_id: self.vue.tehama_info[info_idx].id
            },
            function () {
                self.vue.tehama_info.splice(info_idx, 1);
                enumerate(self.vue.tehama_info);
            })
    };


    //------------------trinity-------------------------
    function get_trinity_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return trinity_info_url + "?" + $.param(pp);
    }

    self.get_trinity_info = function() {
        $.getJSON(get_trinity_info_url(0, 100), function (data) {
            self.vue.trinity_info = data.trinity_info;
            enumerate(self.vue.trinity_info);
        })
    };

    self.add_trinity_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_trinity_info = true;
    };

    self.cancel_trinity_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_trinity_info = false;
    };

    self.add_trinity_info = function () {
        $.post(add_trinity_info_url,
            {
                artist: self.vue.form_trinity_artist,
                city: self.vue.form_trinity_city,
					 url: self.vue.form_trinity_url
            },
            function (data) {
            $.web2py.enableElement($("#add_trinity_info_submit"));
            self.vue.is_adding_trinity_info = false;
            self.vue.trinity_info.unshift(data.trinity_info);
            enumerate(self.vue.trinity_info);
            self.vue.form_trinity_artist = "";
            self.vue.form_trinity_city = "";
				self.vue.form_trinity_url = "";
            });
    };

    self.delete_trinity_info = function(info_idx) {
        $.post(del_trinity_info_url,
            {
                trinity_info_id: self.vue.trinity_info[info_idx].id
            },
            function () {
                self.vue.trinity_info.splice(info_idx, 1);
                enumerate(self.vue.trinity_info);
            })
    };


    //--------------------tulare---------------------
    function get_tulare_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return tulare_info_url + "?" + $.param(pp);
    }

    self.get_tulare_info = function() {
        $.getJSON(get_tulare_info_url(0, 100), function (data) {
            self.vue.tulare_info = data.tulare_info;
            enumerate(self.vue.tulare_info);
        })
    };

    self.add_tulare_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_tulare_info = true;
    };

    self.cancel_tulare_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_tulare_info = false;
    };

    self.add_tulare_info = function () {
        $.post(add_tulare_info_url,
            {
                artist: self.vue.form_tulare_artist,
                city: self.vue.form_tulare_city,
					 url: self.vue.form_tulare_url
            },
            function (data) {
            $.web2py.enableElement($("#add_tulare_info_submit"));
            self.vue.is_adding_tulare_info = false;
            self.vue.tulare_info.unshift(data.tulare_info);
            enumerate(self.vue.tulare_info);
            self.vue.form_tulare_artist = "";
            self.vue.form_tulare_city = "";
				self.vue.form_tulare_url = "";
            });
    };

    self.delete_tulare_info = function(info_idx) {
        $.post(del_tulare_info_url,
            {
                tulare_info_id: self.vue.tulare_info[info_idx].id
            },
            function () {
                self.vue.tulare_info.splice(info_idx, 1);
                enumerate(self.vue.tulare_info);
            })
    };

    //---------------------tuolumne--------------
    function get_tuolumne_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return tuolumne_info_url + "?" + $.param(pp);
    }

    self.get_tuolumne_info = function() {
        $.getJSON(get_tuolumne_info_url(0, 100), function (data) {
            self.vue.tuolumne_info = data.tuolumne_info;
            enumerate(self.vue.tuolumne_info);
        })
    };

    self.add_tuolumne_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_tuolumne_info = true;
    };

    self.cancel_tuolumne_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_tuolumne_info = false;
    };

    self.add_tuolumne_info = function () {
        $.post(add_tuolumne_info_url,
            {
                artist: self.vue.form_tuolumne_artist,
                city: self.vue.form_tuolumne_city,
					 url: self.vue.form_tuolumne_url
            },
            function (data) {
            $.web2py.enableElement($("#add_tuolumne_info_submit"));
            self.vue.is_adding_tuolumne_info = false;
            self.vue.tuolumne_info.unshift(data.tuolumne_info);
            enumerate(self.vue.tuolumne_info);
            self.vue.form_tuolumne_artist = "";
            self.vue.form_tuolumne_city = "";
				self.vue.form_tuolumne_url = "";
            });
    };

    self.delete_tuolumne_info = function(info_idx) {
        $.post(del_tuolumne_info_url,
            {
                tuolumne_info_id: self.vue.tuolumne_info[info_idx].id
            },
            function () {
                self.vue.tuolumne_info.splice(info_idx, 1);
                enumerate(self.vue.tuolumne_info);
            })
    };

    //====================ventura---------------------------
    function get_ventura_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return ventura_info_url + "?" + $.param(pp);
    }

    self.get_ventura_info = function() {
        $.getJSON(get_ventura_info_url(0, 100), function (data) {
            self.vue.ventura_info = data.ventura_info;
            enumerate(self.vue.ventura_info);
        })
    };

    self.add_ventura_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_ventura_info = true;
    };

    self.cancel_ventura_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_ventura_info = false;
    };

    self.add_ventura_info = function () {
        $.post(add_ventura_info_url,
            {
                artist: self.vue.form_ventura_artist,
                city: self.vue.form_ventura_city,
					 url: self.vue.form_ventura_url
            },
            function (data) {
            $.web2py.enableElement($("#add_ventura_info_submit"));
            self.vue.is_adding_ventura_info = false;
            self.vue.ventura_info.unshift(data.ventura_info);
            enumerate(self.vue.ventura_info);
            self.vue.form_ventura_artist = "";
            self.vue.form_ventura_city = "";
				self.vue.form_ventura_url = "";
            });
    };

    self.delete_ventura_info = function(info_idx) {
        $.post(del_ventura_info_url,
            {
                ventura_info_id: self.vue.ventura_info[info_idx].id
            },
            function () {
                self.vue.ventura_info.splice(info_idx, 1);
                enumerate(self.vue.ventura_info);
            })
    };


    //--------------------------yolo
    function get_yolo_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return yolo_info_url + "?" + $.param(pp);
    }

    self.get_yolo_info = function() {
        $.getJSON(get_yolo_info_url(0, 100), function (data) {
            self.vue.yolo_info = data.yolo_info;
            enumerate(self.vue.yolo_info);
        })
    };

    self.add_yolo_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_yolo_info = true;
    };

    self.cancel_yolo_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_yolo_info = false;
    };

    self.add_yolo_info = function () {
        $.post(add_yolo_info_url,
            {
                artist: self.vue.form_yolo_artist,
                city: self.vue.form_yolo_city,
					 url: self.vue.form_yolo_url
            },
            function (data) {
            $.web2py.enableElement($("#add_yolo_info_submit"));
            self.vue.is_adding_yolo_info = false;
            self.vue.yolo_info.unshift(data.yolo_info);
            enumerate(self.vue.yolo_info);
            self.vue.form_yolo_artist = "";
            self.vue.form_yolo_city = "";
				self.vue.form_yolo_url = "";
            });
    };

    self.delete_yolo_info = function(info_idx) {
        $.post(del_yolo_info_url,
            {
                yolo_info_id: self.vue.yolo_info[info_idx].id
            },
            function () {
                self.vue.yolo_info.splice(info_idx, 1);
                enumerate(self.vue.yolo_info);
            })
    };


    //-------------yuba-----------
    function get_yuba_info_url (start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return yuba_info_url + "?" + $.param(pp);
    }

    self.get_yuba_info = function() {
        $.getJSON(get_yuba_info_url(0, 100), function (data) {
            self.vue.yuba_info = data.yuba_info;
            enumerate(self.vue.yuba_info);
        })
    };

    self.add_yuba_info_button = function() {
        $("div#uploader_div").show();
        self.vue.is_adding_yuba_info = true;
    };

    self.cancel_yuba_info_button = function() {
        $("div#uploader_div").hide();
        self.vue.is_adding_yuba_info = false;
    };

    self.add_yuba_info = function () {
        $.post(add_yuba_info_url,
            {
                artist: self.vue.form_yuba_artist,
                city: self.vue.form_yuba_city,
					 url: self.vue.form_yuba_url
            },
            function (data) {
            $.web2py.enableElement($("#add_yuba_info_submit"));
            self.vue.is_adding_yuba_info = false;
            self.vue.yuba_info.unshift(data.yuba_info);
            enumerate(self.vue.yuba_info);
            self.vue.form_yuba_artist = "";
            self.vue.form_yuba_city = "";
				self.vue.form_yuba_url = "";
            });
    };

    self.delete_yuba_info = function(info_idx) {
        $.post(del_yuba_info_url,
            {
                yuba_info_id: self.vue.yuba_info[info_idx].id
            },
            function () {
                self.vue.yuba_info.splice(info_idx, 1);
                enumerate(self.vue.yuba_info);
            })
    };


    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            is_adding_alameda_info: false,
            alameda_info: [],
            form_alameda_artist: null,
            form_alameda_city: null,
            form_alameda_url: null,
            is_adding_alpine_info: false,
            alpine_info: [],
            form_alpine_artist: null,
            form_alpine_city: null,
            form_alpine_url: null,
            is_adding_amador_info: false,
            amador_info: [],
            form_amador_artist: null,
            form_amador_city: null,
            form_amador_url: null,
            is_adding_butte_info: false,
            butte_info: [],
            form_butte_artist: null,
            form_butte_city: null,
            form_butte_url: null,
            is_adding_calaveras_info: false,
            calaveras_info: [],
            form_calaveras_artist: null,
            form_calaveras_city: null,
            form_calaveras_url: null,
            is_adding_colusa_info: false,
            colusa_info: [],
            form_colusa_artist: null,
            form_colusa_city: null,
            form_colusa_url: null,
            is_adding_contracosta_info: false,
            contracosta_info: [],
            form_contracosta_artist: null,
            form_contracosta_city: null,
            form_contracosta_url: null,
            is_adding_delnorte_info: false,
            delnorte_info: [],
            form_delnorte_artist: null,
            form_delnorte_city: null,
            form_delnorte_url: null,
            is_adding_eldorado_info: false,
            eldorado_info: [],
            form_eldorado_artist: null,
            form_eldorado_city: null,
            form_eldorado_url: null,
            is_adding_fresno_info: false,
            fresno_info: [],
            form_fresno_artist: null,
            form_fresno_city: null,
            form_fresno_url: null,
            is_adding_glenn_info: false,
            glenn_info: [],
            form_glenn_artist: null,
            form_glenn_city: null,
            form_glenn_url: null,
            is_adding_humboldt_info: false,
            humboldt_info: [],
            form_humboldt_artist: null,
            form_humboldt_city: null,
            form_humboldt_url: null,
            is_adding_imperial_info: false,
            imperial_info: [],
            form_imperial_artist: null,
            form_imperial_city: null,
            form_imperial_url: null,
            is_adding_inyo_info: false,
            inyo_info: [],
            form_inyo_artist: null,
            form_inyo_city: null,
            form_inyo_url: null,
            is_adding_kern_info: false,
            kern_info: [],
            form_kern_artist: null,
            form_kern_city: null,
            form_kern_url: null,
            is_adding_kings_info: false,
            kings_info: [],
            form_kings_artist: null,
            form_kings_city: null,
            form_kings_url: null,
            is_adding_lake_info: false,
            lake_info: [],
            form_lake_artist: null,
            form_lake_city: null,
            form_lake_url: null,
            is_adding_lassen_info: false,
            lassen_info: [],
            form_lassen_artist: null,
            form_lassen_city: null,
            form_lassen_url: null,
            is_adding_losangeles_info: false,
            losangeles_info: [],
            form_losangeles_artist: null,
            form_losangeles_city: null,
            form_losangeles_url: null,
            is_adding_madera_info: false,
            madera_info: [],
            form_madera_artist: null,
            form_madera_city: null,
            form_madera_url: null,
            is_adding_marin_info: false,
            marin_info: [],
            form_marin_artist: null,
            form_marin_city: null,
            form_marin_url: null,
            is_adding_mariposa_info: false,
            mariposa_info: [],
            form_mariposa_artist: null,
            form_mariposa_city: null,
            form_mariposa_url: null,
            is_adding_mendocino_info: false,
            mendocino_info: [],
            form_mendocino_artist: null,
            form_mendocino_city: null,
            form_mendocino_url: null,
            is_adding_merced_info: false,
            merced_info: [],
            form_merced_artist: null,
            form_merced_city: null,
            form_merced_url: null,
            is_adding_modoc_info: false,
            modoc_info: [],
            form_modoc_artist: null,
            form_modoc_city: null,
            form_modoc_url: null,
            is_adding_mono_info: false,
            mono_info: [],
            form_mono_artist: null,
            form_mono_city: null,
            form_mono_url: null,
            is_adding_monterey_info: false,
            monterey_info: [],
            form_monterey_artist: null,
            form_monterey_city: null,
            form_monterey_url: null,
            is_adding_napa_info: false,
            napa_info: [],
            form_napa_artist: null,
            form_napa_city: null,
            form_napa_url: null,
            is_adding_nevada_info: false,
            nevada_info: [],
            form_nevada_artist: null,
            form_nevada_city: null,
            form_nevada_url: null,
            is_adding_orange_info: false,
            orange_info: [],
            form_orange_artist: null,
            form_orange_city: null,
            form_orange_url: null,
            is_adding_placer_info: false,
            placer_info: [],
            form_placer_artist: null,
            form_placer_city: null,
            form_placer_url: null,
            is_adding_plumas_info: false,
            plumas_info: [],
            form_plumas_artist: null,
            form_plumas_city: null,
            form_plumas_url: null,
            is_adding_riverside_info: false,
            riverside_info: [],
            form_riverside_artist: null,
            form_riverside_city: null,
            form_riverside_url: null,
            is_adding_sacramento_info: false,
            sacramento_info: [],
            form_sacramento_artist: null,
            form_sacramento_city: null,
            form_sacramento_url: null,
            is_adding_sanbenito_info: false,
            sanbenito_info: [],
            form_sanbenito_artist: null,
            form_sanbenito_city: null,
            form_sanbenito_url: null,
            is_adding_sanbernardino_info: false,
            sanbernardino_info: [],
            form_sanbernardino_artist: null,
            form_sanbernardino_city: null,
            form_sanbernardino_url: null,
            is_adding_sandiego_info: false,
            sandiego_info: [],
            form_sandiego_artist: null,
            form_sandiego_city: null,
            form_sandiego_url: null,
            is_adding_sanfrancisco_info: false,
            sanfrancisco_info: [],
            form_sanfrancisco_artist: null,
            form_sanfrancisco_city: null,
            form_sanfrancisco_url: null,
            is_adding_sanjoaquin_info: false,
            sanjoaquin_info: [],
            form_sanjoaquin_artist: null,
            form_sanjoaquin_city: null,
            form_sanjoaquin_url: null,
            is_adding_sanluisobispo_info: false,
            sanluisobispo_info: [],
            form_sanluisobispo_artist: null,
            form_sanluisobispo_city: null,
            form_sanluisobispo_url: null,
            is_adding_sanmateo_info: false,
            sanmateo_info: [],
            form_sanmateo_artist: null,
            form_sanmateo_city: null,
            form_sanmateo_url: null,
            is_adding_santabarbara_info: false,
            santabarbara_info: [],
            form_santabarbara_artist: null,
            form_santabarbara_city: null,
            form_santabarbara_url: null,
            is_adding_santaclara_info: false,
            santaclara_info: [],
            form_santaclara_artist: null,
            form_santaclara_city: null,
            form_santaclara_url: null,
            is_adding_santacruz_info: false,
            santacruz_info: [],
            form_santacruz_artist: null,
            form_santacruz_city: null,
            form_santacruz_url: null,
            is_adding_shasta_info: false,
            shasta_info: [],
            form_shasta_artist: null,
            form_shasta_city: null,
            form_shasta_url: null,
            is_adding_sierra_info: false,
            sierra_info: [],
            form_sierra_artist: null,
            form_sierra_city: null,
            form_sierra_url: null,
            is_adding_siskiyou_info: false,
            siskiyou_info: [],
            form_siskiyou_artist: null,
            form_siskiyou_city: null,
            form_siskiyou_url: null,
            is_adding_solano_info: false,
            solano_info: [],
            form_solano_artist: null,
            form_solano_city: null,
            form_solano_url: null,
            is_adding_sonoma_info: false,
            sonoma_info: [],
            form_sonoma_artist: null,
            form_sonoma_city: null,
            form_sonoma_url: null,
            is_adding_stanislaus_info: false,
            stanislaus_info: [],
            form_stanislaus_artist: null,
            form_stanislaus_city: null,
            form_stanislaus_url: null,
            is_adding_sutter_info: false,
            sutter_info: [],
            form_sutter_artist: null,
            form_sutter_city: null,
            form_sutter_url: null,
            is_adding_tehama_info: false,
            tehama_info: [],
            form_tehama_artist: null,
            form_tehama_city: null,
            form_tehama_url: null,
            is_adding_trinity_info: false,
            trinity_info: [],
            form_trinity_artist: null,
            form_trinity_city: null,
            form_trinity_url: null,
            is_adding_tulare_info: false,
            tulare_info: [],
            form_tulare_artist: null,
            form_tulare_city: null,
            form_tulare_url: null,
            is_adding_tuolumne_info: false,
            tuolumne_info: [],
            form_tuolumne_artist: null,
            form_tuolumne_city: null,
            form_tuolumne_url: null,
            is_adding_ventura_info: false,
            ventura_info: [],
            form_ventura_artist: null,
            form_ventura_city: null,
            form_ventura_url: null,
            is_adding_yolo_info: false,
            yolo_info: [],
            form_yolo_artist: null,
            form_yolo_city: null,
            form_yolo_url: null,
            is_adding_yuba_info: false,
            yuba_info: [],
            form_yuba_artist: null,
            form_yuba_city: null,
            form_yuba_url: null,
        },
        methods: {
            add_alameda_info_button: self.add_alameda_info_button,
            add_alameda_info: self.add_alameda_info,
            delete_alameda_info: self.delete_alameda_info,
            cancel_alameda_info_button: self.cancel_alameda_info_button,
            add_alpine_info_button: self.add_alpine_info_button,
            add_alpine_info: self.add_alpine_info,
            delete_alpine_info: self.delete_alpine_info,
            cancel_alpine_info_button: self.cancel_alpine_info_button,
            add_amador_info_button: self.add_amador_info_button,
            add_amador_info: self.add_amador_info,
            delete_amador_info: self.delete_amador_info,
            cancel_amador_info_button: self.cancel_amador_info_button,
            add_butte_info_button: self.add_butte_info_button,
            add_butte_info: self.add_butte_info,
            delete_butte_info: self.delete_butte_info,
            cancel_butte_info_button: self.cancel_butte_info_button,
            add_calaveras_info_button: self.add_calaveras_info_button,
            add_calaveras_info: self.add_calaveras_info,
            delete_calaveras_info: self.delete_calaveras_info,
            cancel_calaveras_info_button: self.cancel_calaveras_info_button,
            add_colusa_info_button: self.add_colusa_info_button,
            add_colusa_info: self.add_colusa_info,
            delete_colusa_info: self.delete_colusa_info,
            cancel_colusa_info_button: self.cancel_colusa_info_button,
            add_contracosta_info_button: self.add_contracosta_info_button,
            add_contracosta_info: self.add_contracosta_info,
            delete_contracosta_info: self.delete_contracosta_info,
            cancel_contracosta_info_button: self.cancel_contracosta_info_button,
            add_delnorte_info_button: self.add_delnorte_info_button,
            add_delnorte_info: self.add_delnorte_info,
            delete_delnorte_info: self.delete_delnorte_info,
            cancel_delnorte_info_button: self.cancel_delnorte_info_button,
            add_eldorado_info_button: self.add_eldorado_info_button,
            add_eldorado_info: self.add_eldorado_info,
            delete_eldorado_info: self.delete_eldorado_info,
            cancel_eldorado_info_button: self.cancel_eldorado_info_button,
            add_fresno_info_button: self.add_fresno_info_button,
            add_fresno_info: self.add_fresno_info,
            delete_fresno_info: self.delete_fresno_info,
            cancel_fresno_info_button: self.cancel_fresno_info_button,
            add_glenn_info_button: self.add_glenn_info_button,
            add_glenn_info: self.add_glenn_info,
            delete_glenn_info: self.delete_glenn_info,
            cancel_glenn_info_button: self.cancel_glenn_info_button,
            add_humboldt_info_button: self.add_humboldt_info_button,
            add_humboldt_info: self.add_humboldt_info,
            delete_humboldt_info: self.delete_humboldt_info,
            cancel_humboldt_info_button: self.cancel_humboldt_info_button,
            add_imperial_info_button: self.add_imperial_info_button,
            add_imperial_info: self.add_imperial_info,
            delete_imperial_info: self.delete_imperial_info,
            cancel_imperial_info_button: self.cancel_imperial_info_button,
            add_inyo_info_button: self.add_inyo_info_button,
            add_inyo_info: self.add_inyo_info,
            delete_inyo_info: self.delete_inyo_info,
            cancel_inyo_info_button: self.cancel_inyo_info_button,
            add_kern_info_button: self.add_kern_info_button,
            add_kern_info: self.add_kern_info,
            delete_kern_info: self.delete_kern_info,
            cancel_kern_info_button: self.cancel_kern_info_button,
            add_kings_info_button: self.add_kings_info_button,
            add_kings_info: self.add_kings_info,
            delete_kings_info: self.delete_kings_info,
            cancel_kings_info_button: self.cancel_kings_info_button,
            add_lake_info_button: self.add_lake_info_button,
            add_lake_info: self.add_lake_info,
            delete_lake_info: self.delete_lake_info,
            cancel_lake_info_button: self.cancel_lake_info_button,
            add_lassen_info_button: self.add_lassen_info_button,
            add_lassen_info: self.add_lassen_info,
            delete_lassen_info: self.delete_lassen_info,
            cancel_lassen_info_button: self.cancel_lassen_info_button,
            add_losangeles_info_button: self.add_losangeles_info_button,
            add_losangeles_info: self.add_losangeles_info,
            delete_losangeles_info: self.delete_losangeles_info,
            cancel_losangeles_info_button: self.cancel_losangeles_info_button,
            add_madera_info_button: self.add_madera_info_button,
            add_madera_info: self.add_madera_info,
            delete_madera_info: self.delete_madera_info,
            cancel_madera_info_button: self.cancel_madera_info_button,
            add_marin_info_button: self.add_marin_info_button,
            add_marin_info: self.add_marin_info,
            delete_marin_info: self.delete_marin_info,
            cancel_marin_info_button: self.cancel_marin_info_button,
            add_mariposa_info_button: self.add_mariposa_info_button,
            add_mariposa_info: self.add_mariposa_info,
            delete_mariposa_info: self.delete_mariposa_info,
            cancel_mariposa_info_button: self.cancel_mariposa_info_button,
            add_mendocino_info_button: self.add_mendocino_info_button,
            add_mendocino_info: self.add_mendocino_info,
            delete_mendocino_info: self.delete_mendocino_info,
            cancel_mendocino_info_button: self.cancel_mendocino_info_button,
            add_merced_info_button: self.add_merced_info_button,
            add_merced_info: self.add_merced_info,
            delete_merced_info: self.delete_merced_info,
            cancel_merced_info_button: self.cancel_merced_info_button,
            add_modoc_info_button: self.add_modoc_info_button,
            add_modoc_info: self.add_modoc_info,
            delete_modoc_info: self.delete_modoc_info,
            cancel_modoc_info_button: self.cancel_modoc_info_button,
            add_mono_info_button: self.add_mono_info_button,
            add_mono_info: self.add_mono_info,
            delete_mono_info: self.delete_mono_info,
            cancel_mono_info_button: self.cancel_mono_info_button,
            add_monterey_info_button: self.add_monterey_info_button,
            add_monterey_info: self.add_monterey_info,
            delete_monterey_info: self.delete_monterey_info,
            cancel_monterey_info_button: self.cancel_monterey_info_button,
            add_napa_info_button: self.add_napa_info_button,
            add_napa_info: self.add_napa_info,
            delete_napa_info: self.delete_napa_info,
            cancel_napa_info_button: self.cancel_napa_info_button,
            add_nevada_info_button: self.add_nevada_info_button,
            add_nevada_info: self.add_nevada_info,
            delete_nevada_info: self.delete_nevada_info,
            cancel_nevada_info_button: self.cancel_nevada_info_button,
            add_orange_info_button: self.add_orange_info_button,
            add_orange_info: self.add_orange_info,
            delete_orange_info: self.delete_orange_info,
            cancel_orange_info_button: self.cancel_orange_info_button,
            add_placer_info_button: self.add_placer_info_button,
            add_placer_info: self.add_placer_info,
            delete_placer_info: self.delete_placer_info,
            cancel_placer_info_button: self.cancel_placer_info_button,
            add_plumas_info_button: self.add_plumas_info_button,
            add_plumas_info: self.add_plumas_info,
            delete_plumas_info: self.delete_plumas_info,
            cancel_plumas_info_button: self.cancel_plumas_info_button,
            add_riverside_info_button: self.add_riverside_info_button,
            add_riverside_info: self.add_riverside_info,
            delete_riverside_info: self.delete_riverside_info,
            cancel_riverside_info_button: self.cancel_riverside_info_button,
            add_sacramento_info_button: self.add_sacramento_info_button,
            add_sacramento_info: self.add_sacramento_info,
            delete_sacramento_info: self.delete_sacramento_info,
            cancel_sacramento_info_button: self.cancel_sacramento_info_button,
            add_sanbenito_info_button: self.add_sanbenito_info_button,
            add_sanbenito_info: self.add_sanbenito_info,
            delete_sanbenito_info: self.delete_sanbenito_info,
            cancel_sanbenito_info_button: self.cancel_sanbenito_info_button,
            add_sanbernardino_info_button: self.add_sanbernardino_info_button,
            add_sanbernardino_info: self.add_sanbernardino_info,
            delete_sanbernardino_info: self.delete_sanbernardino_info,
            cancel_sanbernardino_info_button: self.cancel_sanbernardino_info_button,
            add_sandiego_info_button: self.add_sandiego_info_button,
            add_sandiego_info: self.add_sandiego_info,
            delete_sandiego_info: self.delete_sandiego_info,
            cancel_sandiego_info_button: self.cancel_sandiego_info_button,
            add_sanfrancisco_info_button: self.add_sanfrancisco_info_button,
            add_sanfrancisco_info: self.add_sanfrancisco_info,
            delete_sanfrancisco_info: self.delete_sanfrancisco_info,
            cancel_sanfrancisco_info_button: self.cancel_sanfrancisco_info_button,
            add_sanjoaquin_info_button: self.add_sanjoaquin_info_button,
            add_sanjoaquin_info: self.add_sanjoaquin_info,
            delete_sanjoaquin_info: self.delete_sanjoaquin_info,
            cancel_sanjoaquin_info_button: self.cancel_sanjoaquin_info_button,
            add_sanluisobispo_info_button: self.add_sanluisobispo_info_button,
            add_sanluisobispo_info: self.add_sanluisobispo_info,
            delete_sanluisobispo_info: self.delete_sanluisobispo_info,
            cancel_sanluisobispo_info_button: self.cancel_sanluisobispo_info_button,
            add_sanmateo_info_button: self.add_sanmateo_info_button,
            add_sanmateo_info: self.add_sanmateo_info,
            delete_sanmateo_info: self.delete_sanmateo_info,
            cancel_sanmateo_info_button: self.cancel_sanmateo_info_button,
            add_santabarbara_info_button: self.add_santabarbara_info_button,
            add_santabarbara_info: self.add_santabarbara_info,
            delete_santabarbara_info: self.delete_santabarbara_info,
            cancel_santabarbara_info_button: self.cancel_santabarbara_info_button,
            add_santaclara_info_button: self.add_santaclara_info_button,
            add_santaclara_info: self.add_santaclara_info,
            delete_santaclara_info: self.delete_santaclara_info,
            cancel_santaclara_info_button: self.cancel_santaclara_info_button,
            add_santacruz_info_button: self.add_santacruz_info_button,
            add_santacruz_info: self.add_santacruz_info,
            delete_santacruz_info: self.delete_santacruz_info,
            cancel_santacruz_info_button: self.cancel_santacruz_info_button,
            add_shasta_info_button: self.add_shasta_info_button,
            add_shasta_info: self.add_shasta_info,
            delete_shasta_info: self.delete_shasta_info,
            cancel_shasta_info_button: self.cancel_shasta_info_button,
            add_sierra_info_button: self.add_sierra_info_button,
            add_sierra_info: self.add_sierra_info,
            delete_sierra_info: self.delete_sierra_info,
            cancel_sierra_info_button: self.cancel_sierra_info_button,
            add_siskiyou_info_button: self.add_siskiyou_info_button,
            add_siskiyou_info: self.add_siskiyou_info,
            delete_siskiyou_info: self.delete_siskiyou_info,
            cancel_siskiyou_info_button: self.cancel_siskiyou_info_button,
            add_solano_info_button: self.add_solano_info_button,
            add_solano_info: self.add_solano_info,
            delete_solano_info: self.delete_solano_info,
            cancel_solano_info_button: self.cancel_solano_info_button,
            add_sonoma_info_button: self.add_sonoma_info_button,
            add_sonoma_info: self.add_sonoma_info,
            delete_sonoma_info: self.delete_sonoma_info,
            cancel_sonoma_info_button: self.cancel_sonoma_info_button,
            add_stanislaus_info_button: self.add_stanislaus_info_button,
            add_stanislaus_info: self.add_stanislaus_info,
            delete_stanislaus_info: self.delete_stanislaus_info,
            cancel_stanislaus_info_button: self.cancel_stanislaus_info_button,
            add_sutter_info_button: self.add_sutter_info_button,
            add_sutter_info: self.add_sutter_info,
            delete_sutter_info: self.delete_sutter_info,
            cancel_sutter_info_button: self.cancel_sutter_info_button,
            add_tehama_info_button: self.add_tehama_info_button,
            add_tehama_info: self.add_tehama_info,
            delete_tehama_info: self.delete_tehama_info,
            cancel_tehama_info_button: self.cancel_tehama_info_button,
            add_trinity_info_button: self.add_trinity_info_button,
            add_trinity_info: self.add_trinity_info,
            delete_trinity_info: self.delete_trinity_info,
            cancel_trinity_info_button: self.cancel_trinity_info_button,
            add_tulare_info_button: self.add_tulare_info_button,
            add_tulare_info: self.add_tulare_info,
            delete_tulare_info: self.delete_tulare_info,
            cancel_tulare_info_button: self.cancel_tulare_info_button,
            add_tuolumne_info_button: self.add_tuolumne_info_button,
            add_tuolumne_info: self.add_tuolumne_info,
            delete_tuolumne_info: self.delete_tuolumne_info,
            cancel_tuolumne_info_button: self.cancel_tuolumne_info_button,
            add_ventura_info_button: self.add_ventura_info_button,
            add_ventura_info: self.add_ventura_info,
            delete_ventura_info: self.delete_ventura_info,
            cancel_ventura_info_button: self.cancel_ventura_info_button,
            add_yolo_info_button: self.add_yolo_info_button,
            add_yolo_info: self.add_yolo_info,
            delete_yolo_info: self.delete_yolo_info,
            cancel_yolo_info_button: self.cancel_yolo_info_button,
            add_yuba_info_button: self.add_yuba_info_button,
            add_yuba_info: self.add_yuba_info,
            delete_yuba_info: self.delete_yuba_info,
            cancel_yuba_info_button: self.cancel_yuba_info_button,



        }

    });


    self.get_alameda_info();
    self.get_alpine_info();
    self.get_amador_info();
    self.get_butte_info();
    self.get_calaveras_info();
    self.get_colusa_info();
    self.get_contracosta_info();
    self.get_delnorte_info();
    self.get_eldorado_info();
    self.get_fresno_info();
    self.get_glenn_info();
    self.get_humboldt_info();
    self.get_imperial_info();
    self.get_inyo_info();
    self.get_kern_info();
    self.get_kings_info();
    self.get_lake_info();
    self.get_lassen_info();
    self.get_losangeles_info();
    self.get_madera_info();
    self.get_marin_info();
    self.get_mariposa_info();
    self.get_mendocino_info();
    self.get_merced_info();
    self.get_modoc_info();
    self.get_mono_info();
    self.get_monterey_info();
    self.get_napa_info();
    self.get_nevada_info();
    self.get_orange_info();
    self.get_placer_info();
    self.get_plumas_info();
    self.get_riverside_info();
    self.get_sacramento_info();
    self.get_sanbenito_info();
    self.get_sanbernardino_info();
    self.get_sandiego_info();
    self.get_sanfrancisco_info();
    self.get_sanjoaquin_info();
    self.get_sanluisobispo_info();
    self.get_sanmateo_info();
    self.get_santabarbara_info();
    self.get_santaclara_info();
    self.get_santacruz_info();
    self.get_shasta_info();
    self.get_sierra_info();
    self.get_siskiyou_info();
    self.get_solano_info();
    self.get_sonoma_info();
    self.get_stanislaus_info();
    self.get_sutter_info();
    self.get_tehama_info();
    self.get_trinity_info();
    self.get_tulare_info();
    self.get_tuolumne_info();
    self.get_ventura_info();
    self.get_yolo_info();
    self.get_yuba_info();

    $("#vue-div").show();


    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});
